# Bomberman Design Patterns

<div align="center">
<img width="300" height="400" src="Bomberman.png">
</div>

## Description du projet

Application bureautique réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Design Patterns" durant l'année 2019-2020 avec un groupe de deux personnes.

Ce projet est composé d’un monde de taille limitée sur lequel évolue un certain nombre d’agents. Le déroulé général du jeu est le suivant :
- Au tour t = 0 le plateau du jeu est initialisé suivant une configuration définie par l’utilisateur. Les agents sont créés et placés sur le plateau.
- A chaque tour t, chaque agent peut réaliser une action prédéfinie par un ensemble de règles et qui a un impact sur l’environnement. \
L’ensemble de ces actions vont conduire à un nouvel état du monde au temps t + 1.
- Une fois que le nombre maximum de tours est atteint ou bien qu’une condition spécifique de fin de jeu est atteinte, le jeu s’arrête.

Nous avons crées un jeu de plateau de type Bomberman qui comprend un ou plusieurs agents Bomberman ainsi que plusieurs types de personnages non joueurs (PNJ).

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'IUT de Nantes :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Anas TAGUENITI : atagueniti@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Olivier GOUDET : olivier.goudet@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Design Patterns" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 20/12/2019.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- Principes SOLID ;
- Design Patterns (MVC, Fabrique, Singleton, Observateur, Stratégie, Adapteur, etc.).

## Objectifs

L’objectif de ce projet est d’implémenter un jeu de Bomberman interactif avec une interface visuelle
de la façon la plus modulable et extensible possible en utilisant les Design Patterns.
Une fois la base du jeu réalisée, nous avons implémenté les comportements et les stratégies des
agents pour remporter la partie. Nous avons implémenté également de l'intelligence artificielle
pour guider l'utilisateur dans la victoire.