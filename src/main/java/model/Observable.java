package model;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import view.Observer;

public class Observable
{
   private boolean changed;
 
   /* List of the Observers registered as interested in this Observable. */
   private LinkedHashSet<Observer> observers;
 

   public Observable()
   {
     observers = new LinkedHashSet<>();
   }

   public synchronized void addObserver(Observer observer)
   {
     if (observer == null)
       throw new NullPointerException("can't add null observer");
     observers.add(observer);
   }

   protected synchronized void clearChanged()
   {
     changed = false;
   }

   public synchronized int countObservers()
   {
     return observers.size();
   }
 
   public synchronized void deleteObserver(Observer victim)
   {
     observers.remove(victim);
   }

   public synchronized void deleteObservers()
   {
     observers.clear();
   }
 

   public synchronized boolean hasChanged()
   {
     return changed;
   }

   public void notifyObservers()
   {
     notifyObservers(null);
   }
 
   public void notifyObservers(Object obj)
   {
     if (! hasChanged())
       return;
     Set<?> s;
     synchronized (this)
       {
         s = (Set) observers.clone();
       }
     int i = s.size();
     Iterator<?> iter = s.iterator();
     while (--i >= 0)
       ((Observer) iter.next()).update(this, obj);
     clearChanged();
   }
 

   protected synchronized void setChanged()
   {
     changed = true;
   }
 }
