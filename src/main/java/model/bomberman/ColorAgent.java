package model.bomberman;

/**
 * Enumération des couleurs
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public enum ColorAgent {
	BLEU,ROUGE,VERT,JAUNE,BLANC,DEFAULT
}
