package model.bomberman;

import controller.bomberman.checkable.CheckRuleApplyBonus;
import controller.bomberman.checkable.CheckRuleAttackBomb;
import controller.bomberman.checkable.CheckRuleAttackEnnemy;
import controller.bomberman.checkable.CheckRuleComingBombermanToBird;
import controller.bomberman.checkable.CheckRuleMoveAgentEnemyRandomly;
import controller.bomberman.checkable.RuleType;

/**
 * Facotry pour construire le jeu en implémentant les règles
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class FactoryBombermanGame {

	/**
	 * Constructeur privé pour éviter l'instanciation de cette factory
	 */
	private FactoryBombermanGame() {
		
	}
	
	/**
	 * Retourne une instance de jeu pour le joueur
	 * Toutes les règles sont à enregistrés ici
	 * @param maxturn
	 * @param time
	 * @return
	 */
	public static BombermanGame getBombermanGameForPlayer(int maxturn, long time) {
		BombermanGame bombermanGame = new BombermanGame(maxturn, time);
		
		bombermanGame.getRules().put(RuleType.ATTACK_BOMB, new CheckRuleAttackBomb());
		bombermanGame.getRules().put(RuleType.ATTACK_ENNEMY, new CheckRuleAttackEnnemy());
		bombermanGame.getRules().put(RuleType.APPLY_BONUS, new CheckRuleApplyBonus());
		bombermanGame.getRules().put(RuleType.FLY_BIRD, new CheckRuleComingBombermanToBird());
		bombermanGame.getRules().put(RuleType.ENNEMY_MOVE_RANDOMLY, new CheckRuleMoveAgentEnemyRandomly());
		
		return bombermanGame;
	}
	
	/**
	 * Retourne une instance de jeu pour la recherche en IA
	 * Seul les régles d'attaque sur les gentils et d'explosion des bombes sont activés
	 * En effet le déplacement aléatoire des ennemis restent compliqué à gérer
	 * @param maxturn
	 * @param time
	 * @return
	 */
	public static BombermanGame getBombermanGameForIA(int maxturn, long time) {
		BombermanGame bombermanGame = new BombermanGame(maxturn, time);
		
		bombermanGame.getRules().put(RuleType.ATTACK_BOMB, new CheckRuleAttackBomb());
		bombermanGame.getRules().put(RuleType.ATTACK_ENNEMY, new CheckRuleAttackEnnemy());
		
		return bombermanGame;
	}
}
