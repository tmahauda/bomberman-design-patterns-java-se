package model.bomberman;

import java.util.ArrayList;
import controller.bomberman.actionnable.AgentAction;
import controller.bomberman.actionnable.StateBomb;
import controller.bomberman.moveable.AgentMove;
import ia.search.Node;
import model.Game;
import model.bomberman.agent.Agent;
import model.bomberman.agent.InfoAgent;
import model.bomberman.agent.enemy.AgentEnemy;
import model.bomberman.agent.factory.AgentFactory;
import model.bomberman.agent.factory.AgentFactoryProvider;
import model.bomberman.agent.kind.AgentKind;
import model.bomberman.map.Map;
import model.bomberman.map.MapAdapter;

/**
 * Classe qui hérite Game et implémente les régles du jeu Bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class BombermanGame extends Game<MapAdapter> {
	
	private static final long serialVersionUID = 1L;
	
	public BombermanGame(int maxturn, long time) {
		super(maxturn, time);
	}
	
	@Override
	public void initializeGame() {
		//Si la map n'est par chargé, on ne peut pas initialiser le jeu
		if(Map.getInstance().getStart_Agents().isEmpty()) return;
				
		//On crée les différents agents avec le DP Factory
		for(InfoAgent infoAgent : Map.getInstance().getStart_Agents()) {
			AgentFactory agentFactory = AgentFactoryProvider.getAgentFactory(infoAgent.getType());
			Agent agent = agentFactory.createAgent(infoAgent);
			
			if(agent != null)
				Map.getInstance().getAgents().add(agent);
				
			if(agent instanceof AgentKind)
				Map.getInstance().getAgentsKind().add((AgentKind)agent);
					
			if(agent instanceof AgentEnemy)
				Map.getInstance().getAgentsEnnemy().add((AgentEnemy)agent);
			}
				
			//Pour l'instant on prend le premier bomberman
			Map.getInstance().setCurrentAgentKind(Map.getInstance().getAgentsKind().get(0));
			
			//On change le tableau de la map avec cette nouvelle liste d'agent
			//afin de les manipuler par référence
			Map.getInstance().setStart_Agents(new ArrayList<>(Map.getInstance().getAgents()));
	}
	

	@Override
	public void findSoluce(Node<MapAdapter> node) {
		if(node == null) return;
		
		//Si l'action est un mouvement
		String action = node.getAction();
		if(action == null || action.equals("")) return;
		
		AgentMove agentMove = null;
		AgentAction agentAction = null;
		
		try {
			agentMove = AgentMove.valueOf(action);
		} 
		catch(IllegalArgumentException e) {
			
		}
		
		try {
			agentAction = AgentAction.valueOf(action);
		} 
		catch(IllegalArgumentException e) {
			
		}
		
		if(agentMove != null) {
			Map.getInstance().getCurrentAgentKind().doMove(agentMove);	
		}
		if(agentAction != null) {
			//Dans le cas de poser une bombe on la fait exploser immédiatement
			if(agentAction == AgentAction.PUT_BOMB) {
				StateBomb stateBomb = Map.getInstance().getCurrentAgentKind().getStateBomb();
				Map.getInstance().getCurrentAgentKind().setStateBomb(StateBomb.Boom);
				Map.getInstance().getCurrentAgentKind().doAction(agentAction);
				Map.getInstance().getCurrentAgentKind().setStateBomb(stateBomb);
			}
		}
	}

	/**
	 * La partie est terminé. Soit parce qu'il a perdu ou gagné
	 * Il a perdu s'il ne peut plus continuer à jouer
	 * Il a gagné s'il peut continuer à jouer
	 */
	@Override
	public boolean gameOver() {		
		return Map.getInstance().getAgentsKind().isEmpty() || this.turn >= this.maxturn;	
	}
	
	/**
	 * Le jeu continue tant qu'il y a des agents sur le plateau
	 */
	@Override
	public boolean gameContinue() {
		return !Map.getInstance().getAgentsEnnemy().isEmpty() && !Map.getInstance().getAgentsKind().isEmpty();
	}
}
