package model.bomberman.map;

import java.util.ArrayList;
import java.util.List;
import controller.bomberman.actionnable.StateBomb;
import model.bomberman.agent.kind.AgentKind;
import model.bomberman.agent.kind.AgentKindBomberman;

/**
 * Factory qui permet de créer une map pour la recherche en IA en proposant
 * - Un état initial
 * - Des états finaux
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class FactoryMap {
	
	private FactoryMap() {
		
	}

	/**
	 * Créer l'état initial de la map
	 * @param path
	 * @return
	 */
	public static MapAdapter getMapIAIntial(int maxturn, long time, String path, List<Integer> actions) {
		try {
			MapAdapter bombermanInitial = new MapAdapter(maxturn, time, actions);
			bombermanInitial.setFilename(path);
			bombermanInitial.load();
			bombermanInitial.initialize();
			
			//On met l'état des bombes à boom
			for(AgentKind kind : bombermanInitial.getAgentsKind()) {
				kind.setStateBomb(StateBomb.Boom);
			}
			
			return bombermanInitial;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Créer les états finaux à partir de la map initial
	 * @param path
	 * @return
	 */
	public static List<MapAdapter> getListMapIAFinals(MapAdapter bombermanInitial) {
		//Etats finaux : tous les agents sont morts et tous les bombermans peuvent être sur n'importeoù sur le plateau
		//On crée autant d'état qu'il existe de bomberman
		List<MapAdapter> bombermanFinals = new ArrayList<>();
		
		for(AgentKind agentKind : bombermanInitial.getAgentsKind()) {
			for(int x=0; x<bombermanInitial.getSize_x(); x++) {
				for(int y=0; y<bombermanInitial.getSize_y(); y++) {
					//On crée les agents bomberman disposés sur toutes les cases. Le plus important est d'avoir éliminé les méchants
					AgentKind newAgentKind = new AgentKindBomberman(x, y, agentKind.getAgentMove(), agentKind.getType(), agentKind.getColor(),
							agentKind.isInvincible(), agentKind.isSick(), StateBomb.Boom);
					
					//On crée une copie profonde de la map intial pour éviter de pointer sur les memes références
					MapAdapter bombermanFinal = (MapAdapter)bombermanInitial.clone();
					
					//On supprime tous les agent
					bombermanFinal.getStart_Agents().clear(); //Affichage
					bombermanFinal.getAgents().clear(); //Hiérarchie
					bombermanFinal.getAgentsEnnemy().clear(); //Méchant
					bombermanFinal.getAgentsKind().clear(); //Gentil
					
					//On ajoute les gentils bomberman
					bombermanFinal.getStart_Agents().add(newAgentKind);
					bombermanFinal.getAgents().add(newAgentKind);
					bombermanFinal.getAgentsKind().add(newAgentKind);
					bombermanFinal.setCurrentAgentKind(newAgentKind);
					
					//On ajoute le plateau dans les états finaux
					bombermanFinals.add(bombermanFinal);
				}
			}
		}
		
		return bombermanFinals;
	}
}
