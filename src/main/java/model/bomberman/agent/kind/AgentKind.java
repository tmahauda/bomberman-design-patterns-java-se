package model.bomberman.agent.kind;

import java.util.HashMap;
import controller.bomberman.itemable.ItemType;
import controller.bomberman.itemable.Itemable;
import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import controller.bomberman.actionnable.StateBomb;
import model.bomberman.agent.Agent;
import model.bomberman.agent.CapacityItem;
import model.bomberman.agent.InfoAgent;

/**
 * Agent gentil qui peut posséder des bonus ou malus
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public abstract class AgentKind extends Agent {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Toutes les bonus réalisables par les agents gentils
	 */
	protected java.util.Map<ItemType, Itemable> bonus;
	
	public AgentKind(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		super(x, y, move, type, color, isInvincible, isSick);
		this.bonus = new HashMap<>();
		this.stateBomb = stateBomb;
	}
	
	public AgentKind(InfoAgent infoAgent, StateBomb stateBomb) {
		super(infoAgent);
		this.bonus = new HashMap<>();
		this.stateBomb = stateBomb;
	}
	
	/**
	 * Le bonus à réaliser sur le bomberman
	 * @param agentAction
	 */
	public boolean isLegalBonus(ItemType itemType) {
		if(itemType == null) return false;
		if(!this.bonus.containsKey(itemType)) return false;
		
		Itemable item = this.bonus.get(itemType);
		if(item == null) return false;
		
		return item.isLegalBonus(this, itemType);
	}
	
	/**
	 * Le bonus à réaliser sur le bomberman
	 * @param agentAction
	 */
	public void doBonus(ItemType itemType) {
		if(itemType == null) return;
		if(!this.bonus.containsKey(itemType)) return;
		
		Itemable item = this.bonus.get(itemType);
		if(item == null) return;
		
		if(item.isLegalBonus(this, itemType)) {
			item.doBonus(this, itemType);
		}
	}
	
	/**
	 * Vérifier si le bomberman est en vie
	 * Le bomberman est mort lorsqu'il n'a plus de vie
	 * @return
	 */
	public boolean isDead() {
		CapacityItem life = this.getCapacityItem(ItemType.LIFE);
		if(life == null) return false;
		else return life.getCapacityActualItem() == 0;
	}

	/**
	 * @return the bonus
	 */
	public java.util.Map<ItemType, Itemable> getBonus() {
		return bonus;
	}

	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(java.util.Map<ItemType, Itemable> bonus) {
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "AgentKind [stateBomb=" + stateBomb + ", actions=" + actions + ", bonus=" + bonus + "]";
	}
}
