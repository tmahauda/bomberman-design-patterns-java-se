package model.bomberman.agent.kind;

import controller.bomberman.actionnable.StateBomb;
import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import model.bomberman.agent.InfoAgent;

/**
 * Agent bomberman, poseur de bombe
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentKindBomberman extends AgentKind {

	private static final long serialVersionUID = 1L;

	public AgentKindBomberman(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		super(x, y, move, type, color, isInvincible, isSick, stateBomb);
	}
	
	public AgentKindBomberman(InfoAgent infoAgent, StateBomb stateBomb) {
		super(infoAgent, stateBomb);
	}
}
