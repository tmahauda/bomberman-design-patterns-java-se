package model.bomberman.agent.factory;

import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import model.bomberman.agent.Agent;
import model.bomberman.agent.InfoAgent;

/**
 * Factory qui permet de créer un agent
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public interface AgentFactory {
	
	public Agent createAgent(InfoAgent agent);
	public Agent createAgent(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick);
}
