package model.bomberman.agent.factory;

import controller.bomberman.actionnable.AgentAction;
import controller.bomberman.actionnable.AgentActionnableAttack;
import controller.bomberman.itemable.ItemType;
import controller.bomberman.moveable.AgentMove;
import controller.bomberman.moveable.AgentMoveable;
import controller.bomberman.moveable.AgentMoveableChase;
import controller.bomberman.moveable.AgentMoveableStill;
import model.bomberman.ColorAgent;
import model.bomberman.agent.Agent;
import model.bomberman.agent.CapacityItem;
import model.bomberman.agent.InfoAgent;
import model.bomberman.agent.enemy.AgentEnemyBasic;
import model.bomberman.agent.enemy.AgentEnemyBird;
import model.bomberman.agent.enemy.AgentEnemyRajion;

/**
 * Factory qui permet de créer des agents ennemis
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentFactoryEnemy implements AgentFactory {
	
	private static AgentFactoryEnemy instance = null;
	
	private AgentFactoryEnemy() {}
	
	public static AgentFactoryEnemy getInstance()
	{
		if(instance == null)
			instance = new AgentFactoryEnemy();
		
		return instance;
	}
	
	@Override
	public Agent createAgent(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick) {
		
		Agent agent = null;
		CapacityItem attack = null;
		
		switch(type) {
			case AgentFactoryProvider.TYPE_AGENT_BASIQUE:
				agent = new AgentEnemyBasic(x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 1
				attack = new CapacityItem(1, 10, 1, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveable());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_RAJION:
				agent = new AgentEnemyRajion(x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 2
				attack = new CapacityItem(2, 10, 2, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableChase());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_BIRD:
				agent = new AgentEnemyBird(x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 3
				attack = new CapacityItem(3, 10, 3, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableStill());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			default:
				return agent;
		}
	}

	@Override
	public Agent createAgent(InfoAgent infoAgent) {
		
		Agent agent = null;
		CapacityItem attack = null;
		
		switch(infoAgent.getType()) {
			case AgentFactoryProvider.TYPE_AGENT_BASIQUE:
				agent = new AgentEnemyBasic(infoAgent);
				//Attaque ayant un dégat au minimum de 1
				attack = new CapacityItem(1, 10, 1, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveable());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_RAJION:
				agent = new AgentEnemyRajion(infoAgent);
				//Attaque ayant un dégat au minimum de 2
				attack = new CapacityItem(2, 10, 2, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableChase());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_BIRD:
				agent = new AgentEnemyBird(infoAgent);
				//Attaque ayant un dégat au minimum de 3
				attack = new CapacityItem(3, 10, 3, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableStill());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			default:
				return agent;
		}
	}
}
