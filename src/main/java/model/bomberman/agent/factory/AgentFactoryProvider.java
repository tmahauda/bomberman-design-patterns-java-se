package model.bomberman.agent.factory;

/**
 * Les différents type d'agent qui retourne une factory ennemy ou gentil
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public abstract class AgentFactoryProvider {
	
	public static final char TYPE_AGENT_RAJION = 'R';
	public static final char TYPE_AGENT_BIRD = 'V';
	public static final char TYPE_AGENT_BASIQUE = 'E';
	public static final char TYPE_AGENT_BOMBERMAN = 'B';
	
	public static AgentFactory getAgentFactory(char typeAgent) {
		if(isGentil(typeAgent)){
			return AgentFactoryKind.getInstance();
		}
		else{
			return AgentFactoryEnemy.getInstance();
		}
	}
	
	private static boolean isGentil(char typeAgent) {
		return typeAgent == TYPE_AGENT_BOMBERMAN;
	}
}