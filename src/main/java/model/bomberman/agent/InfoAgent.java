package model.bomberman.agent;

import java.io.Serializable;

import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;

/**
 * Les caractéristiques de chaque agent
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class InfoAgent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected int x;
	protected int y;
	protected AgentMove agentMove;
	protected ColorAgent color;
	protected char type;
	protected boolean isInvincible;
	protected boolean isSick;
	
	public InfoAgent(int x, int y, AgentMove agentMove, char type, ColorAgent color, boolean isInvincible, boolean isSick) {
		this.x=x;
		this.y=y;
		this.agentMove = agentMove;
		this.color = color;
		this.type = type;
		this.isInvincible = isInvincible;
		this.isSick = isSick;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the agentMove
	 */
	public AgentMove getAgentMove() {
		return agentMove;
	}

	/**
	 * @param agentMove the agentMove to set
	 */
	public void setAgentMove(AgentMove agentMove) {
		this.agentMove = agentMove;
	}

	/**
	 * @return the color
	 */
	public ColorAgent getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(ColorAgent color) {
		this.color = color;
	}

	/**
	 * @return the type
	 */
	public char getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * @return the isInvincible
	 */
	public boolean isInvincible() {
		return isInvincible;
	}

	/**
	 * @param isInvincible the isInvincible to set
	 */
	public void setInvincible(boolean isInvincible) {
		this.isInvincible = isInvincible;
	}

	/**
	 * @return the isSick
	 */
	public boolean isSick() {
		return isSick;
	}

	/**
	 * @param isSick the isSick to set
	 */
	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoAgent other = (InfoAgent) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoAgent [x=" + x + ", y=" + y + ", agentMove=" + agentMove + ", color=" + color + ", type=" + type
				+ ", isInvincible=" + isInvincible + ", isSick=" + isSick + "]";
	}
}
	