package model.bomberman.agent;

import java.io.Serializable;

/**
 * Item que possède les agents
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class CapacityItem implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Nombre d'item que peut posséder au minimum l'agent
	 */
	private int capacityMinItem;
	
	/**
	 * Nombre d'item que peut posséder au maximum l'agent
	 */
	private int capacityMaxItem;
	
	/**
	 * Nombre d'item que possède actuellement l'agent
	 */
	private int capacityActualItem;

	/**
	 * Pas d'incrément
	 */
	private int step;
	
	public CapacityItem(int capacityMinItem, int capacityMaxItem, int capacityActualItem, int step) {
		this.capacityMinItem = capacityMinItem;
		this.capacityMaxItem = capacityMaxItem;
		this.capacityActualItem = capacityActualItem;
		this.step = step;
	}

	/**
	 * Mettre la capacité actuel au minimum
	 */
	public void putCapacityActualItemToMin() {
		this.capacityActualItem = this.capacityMinItem;
	}
	
	/**
	 * Mettre la capacité actuel au maximum
	 */
	public void putCapacityActualItemToMax() {
		this.capacityActualItem = this.capacityMaxItem;
	}
	
	/**
	 * Incrémente la capacité actuel de l'item
	 * La capacité ne doit dépasser la capacité maximal
	 */
	public void incrementCapacityActualItem() {
		if(this.capacityActualItem + this.step <= this.capacityMaxItem)
			this.capacityActualItem += step;
		else
			this.capacityActualItem = this.capacityMaxItem;
	}
	
	/**
	 * Décrémente la capacité actuel de l'item
	 * La capacité ne doit être en dessous de la capacité minimal
	 */
	public void decrementCapacityActualItem() {
		if(this.capacityActualItem - this.step >= this.capacityMinItem)
			this.capacityActualItem -= step;
		else
			this.capacityActualItem = this.capacityMinItem;
	}
	
	/**
	 * @return the capacityMinItem
	 */
	public int getCapacityMinItem() {
		return capacityMinItem;
	}

	/**
	 * @param capacityMinItem the capacityMinItem to set
	 */
	public void setCapacityMinItem(int capacityMinItem) {
		this.capacityMinItem = capacityMinItem;
	}

	/**
	 * @return the capacityMaxItem
	 */
	public int getCapacityMaxItem() {
		return capacityMaxItem;
	}

	/**
	 * @param capacityMaxItem the capacityMaxItem to set
	 */
	public void setCapacityMaxItem(int capacityMaxItem) {
		this.capacityMaxItem = capacityMaxItem;
	}

	/**
	 * @return the capacityActualItem
	 */
	public int getCapacityActualItem() {
		return capacityActualItem;
	}

	/**
	 * @param capacityActualItem the capacityActualItem to set
	 */
	public void setCapacityActualItem(int capacityActualItem) {
		this.capacityActualItem = capacityActualItem;
	}

	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}

	/**
	 * @param step the step to set
	 */
	public void setStep(int step) {
		this.step = step;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacityActualItem;
		result = prime * result + capacityMaxItem;
		result = prime * result + capacityMinItem;
		result = prime * result + step;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CapacityItem other = (CapacityItem) obj;
		if (capacityActualItem != other.capacityActualItem)
			return false;
		if (capacityMaxItem != other.capacityMaxItem)
			return false;
		if (capacityMinItem != other.capacityMinItem)
			return false;
		if (step != other.step)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CapacityItem [capacityMinItem=" + capacityMinItem + ", capacityMaxItem=" + capacityMaxItem
				+ ", capacityActualItem=" + capacityActualItem + ", step=" + step + "]";
	}
}
