package model.bomberman.agent.enemy;

import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import model.bomberman.agent.InfoAgent;

/**
 * Agent ennemi basique
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentEnemyBasic extends AgentEnemy {

	private static final long serialVersionUID = 1L;

	public AgentEnemyBasic(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick) {
		super(x, y, move, type, color, isInvincible, isSick);
	}
	
	public AgentEnemyBasic(InfoAgent infoAgent) {
		super(infoAgent);
	}
}
