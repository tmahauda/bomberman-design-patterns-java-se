package model.bomberman.agent.enemy;

import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import model.bomberman.agent.InfoAgent;

/**
 * Source : https://bomberman.fandom.com/wiki/Bird_(Bomberman_%2793)
 * Bird moves quickly and erratically. It can fly freely over wall
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentEnemyBird extends AgentEnemy {
	
	private static final long serialVersionUID = 1L;

	public AgentEnemyBird(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick) {
		super(x, y, move, type, color, isInvincible, isSick);
	}
	
	public AgentEnemyBird(InfoAgent infoAgent) {
		super(infoAgent);
	}
}
