package model.bomberman.agent.enemy;

import controller.bomberman.moveable.AgentMove;
import model.bomberman.ColorAgent;
import model.bomberman.agent.InfoAgent;

/**
 * Source : https://bomberman.fandom.com/wiki/Rajion
 * Rajion constantly pursues Bomberman and will harm the player on collision. 
 * Rajion also pauses momentarily and shoots electricity to stun the player 
 * and exhausts itself after before resuming
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentEnemyRajion extends AgentEnemy {

	private static final long serialVersionUID = 1L;

	public AgentEnemyRajion(int x, int y, AgentMove move, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick) {
		super(x, y, move, type, color, isInvincible, isSick);
	}
	
	public AgentEnemyRajion(InfoAgent infoAgent) {
		super(infoAgent);
	}
}
