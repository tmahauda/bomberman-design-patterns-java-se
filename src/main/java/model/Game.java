package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Observable;
import controller.bomberman.checkable.Checkable;
import controller.bomberman.checkable.RuleType;
import ia.search.Node;
import ia.solve.Solveable;

/**
 * Classe abstraite qui hérite Observable et implémente Runnable pour :
 * - Jouer le rôle de sujet afin de notifier les observateurs (les vues) qu'elle a changée d'état à l'aide du pattern Observateur ;
 * - Créer une architecture générale d’un jeu séquentiel à l’aide du pattern Patron de méthode ;
 * - Garder la main sur l’interface graphique pendant que le jeu s’éxécute en tâche de fond, comme le contrôle du temps de la simulation du jeu,
 *   ainsi que de lancer un grand nombre de simulations de jeu en parallèle pour évaluer différentes stratégies de comportement des agents.
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public abstract class Game<T extends Solveable> extends Observable implements Runnable, Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Les règles du jeu
	 */
	protected Map<RuleType, Checkable> rules;
	
	/**
	 * La solution étape par étape
	 */
	protected LinkedList<Node<T>> soluces;
	
	/**
     * Compter le nombre de tour courant
     */
	protected int turn;
	
	/**
     * Le nombre de tour maximum
     */
	protected int maxturn;
	
	/**
     * Savoir si le jeu est lancé
     * Jeu lancé = true
     * Jeu en pause = false
     */
	protected boolean isRunning;
	
	/**
     * Thread du jeu
     */
	protected Thread thread;
	
	/**
     * Corresponds au temps de pause entre chaque tour en millisecondes.
     */
	protected long time;
	
	/**
	 * Faut-il résoudre le jeu avec une IA ?
	 */
	protected boolean soluce;

	/**
     * Constructeur de Game
     * @param maxturn le nombre de tour maximum
     * @param time le temps d'execution entre chaque tour
     */
	public Game(int maxturn, long time) {		
		this.maxturn = maxturn;
		this.time = time;
		this.rules = new HashMap<>();
		this.soluces = new LinkedList<>();
	}
	
	/**
     * Méthode qui permet d'intialiser le jeu
     */
	public abstract void initializeGame();
	
	/**
     * Traitement à faire entre chaque tour
     */
	public void takeTurn(boolean soluce) {
		//On récupère la solution trouvé par l'IA pour ce tour
		Node<T> node = null;
		
		if(!this.soluces.isEmpty())
			node = this.soluces.removeFirst();
		
		//Si on souhaite connaitre la solution alors on l'effectue
		if(soluce) {
			this.findSoluce(node);
		}
		
		//On execute toutes les règles du jeu à chaque tour si possible
		for(Checkable rule : this.rules.values()) {
			if(rule.checkRule())
				rule.executeRule();
		}		
	}
	
	/**
	 * Effectuer la solution trouvé
	 * @param node
	 */
	public abstract void findSoluce(Node<T> node);
	
	/**
     * Message de fin de jeu
     */
	public abstract boolean gameOver();
		
	/**
     * Savoir si le jeu est terminé
     */
	public abstract boolean gameContinue();
	
	/**
	 * Méthode qui initialise le jeu en remettant le compteur du nombre de tours turn à zéro, 
	 * met isRunning à la valeur false et appelle la méthode abstraite void initializeGame(), 
	 * non implémentée pour l’instant. Elle sera implémentée par les classes concrètes qui héritent de Game.
	 */
	public void init() {
		// au debut y pas de tours
		this.turn = 0;
		
		// au debut le jeu n'est pas en route
		this.isRunning = false;
		
		//Si un thread est présent on le détruit
		//Probleme il continue
//		if(this.thread != null)
//			this.thread.destroy();
//			this.thread = null;
			
		this.initializeGame();
	}
	
	/**
	 * Méthode qui incrémente le compteur de tour du jeu et effectue
	 * un seul tour du jeu en appelant la méthode abstraite void takeTurn() si le jeu n’est pas
	 * terminé. Si le jeu est terminé, elle met le booléen isRunning à la valeur false
	 * puis fait appel à la méthode abstraite void gameOver() et qui permettra d’afficher un
	 * message de fin du jeu. Pour savoir si le jeu est terminé, elle appel la méthode abstraite
	 * boolean gameContinue() et vérifie si le nombre maximum de tours est atteint.
	 */
	@SuppressWarnings("deprecation")
	public void step(boolean soluce) {
		
		// Si le jeu n'est pas terminé
		if(this.gameContinue() && this.turn < this.maxturn) {
			this.turn++;				
			this.takeTurn(soluce);
			this.setChanged();
			this.notifyObservers(false);
		}
		// si le jeu est terminé
		else {
			this.gameOver();
			this.isRunning = false;
			this.setChanged();
			this.notifyObservers(true);
			this.thread.destroy();
		}
	}
	
	/**
	 * Méthode qui lance le jeu en pas à pas avec la méthode step() jusqu’à
	 * la fin tant que le jeu n’est pas mis en pause (testé par un flag booléen isRunning).
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		while(this.isRunning) {
			//Pause pour réguler l'execution du jeu entre chaque tour
			try {
				Thread.sleep(this.time);
				
				//Puis execute le tour
				this.step(this.soluce);
			} 
			catch (InterruptedException e) {
				this.isRunning = false;
				this.thread.destroy();
			}
		}		
	}
	
	/**
	 * Méthode concrète stop() qui met en pause le jeu en mettant le flag booléen isRunning à false.
	 */
	public void stop() {
		this.isRunning = false;
	}
	
	/**
	 * Méthode qui met l’attribut isRunning à la valeur true puis instancie l’attribut thread 
	 * avec un nouvel objet Thread qui contient le jeu courant (thread = new Thread(this);) 
	 * et enfin fait appel à la méthode start() de cet objet thread pour lancer le jeu. 
	 * Remarque : l’appel à cette méthode lancera automatiquement la méthode run().
	 */
	public void launch(boolean soluce) {
		this.soluce = soluce;
		this.isRunning = true;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * @return the turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}

	/**
	 * @return the maxturn
	 */
	public int getMaxturn() {
		return maxturn;
	}

	/**
	 * @param maxturn the maxturn to set
	 */
	public void setMaxturn(int maxturn) {
		this.maxturn = maxturn;
	}

	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * @param isRunning the isRunning to set
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the thread
	 */
	public Thread getThread() {
		return thread;
	}

	/**
	 * @param thread the thread to set
	 */
	public void setThread(Thread thread) {
		this.thread = thread;
	}

	/**
	 * @return the rules
	 */
	public Map<RuleType, Checkable> getRules() {
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(Map<RuleType, Checkable> rules) {
		this.rules = rules;
	}

	/**
	 * @return the soluces
	 */
	public LinkedList<Node<T>> getSoluces() {
		return soluces;
	}

	/**
	 * @param soluces the soluces to set
	 */
	public void setSoluces(LinkedList<Node<T>> soluces) {
		this.soluces = soluces;
	}

	/**
	 * @return the soluce
	 */
	public boolean isSoluce() {
		return soluce;
	}

	/**
	 * @param soluce the soluce to set
	 */
	public void setSoluce(boolean soluce) {
		this.soluce = soluce;
	}
}