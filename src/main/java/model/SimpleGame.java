package model;

import ia.search.Node;

/**
 * Classe qui hérite Game et effectue des sorties consoles pour chaque méthode abstraite redéfinie.
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class SimpleGame extends Game {
	
	private static final long serialVersionUID = 1L;

	/**
     * Constructeur de SimpleGame
     * @param maxturn le nombre de tour maximum
     * @param time le temps d'execution entre chaque tour
     */
	public SimpleGame(int maxturn, long time) {
		super(maxturn, time);
	}

	@Override
	public void initializeGame() {
		System.out.println("Initialize game");
	}

	@Override
	public void takeTurn(boolean soluce) {
		System.out.println("Tour " + this.turn + " du jeu en cours");
	}

	@Override
	public boolean gameOver() {
		System.out.println("Fin de jeu");
		return false;
	}

	@Override
	public boolean gameContinue() {
		return true;
	}

	@Override
	public void findSoluce(Node node) {
		// TODO Auto-generated method stub
		
	}
}
