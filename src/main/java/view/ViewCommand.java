package view;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.Observable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;
import controller.Controller;
import controller.bomberman.ControllerBombermanGame;
import ia.solve.Solveable;
import model.Game;

/**
 * Vue qui affiche les différentes commandes pour interagir avec la map bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ViewCommand extends View {
	
	private static final long serialVersionUID = 1L;
	private JButton buttonStart;
	private JButton buttonRun;
	private JButton buttonStep;
	private JButton buttonStop;
	private JButton buttonSolve;
	private JButton buttonChoose;
	private JButton buttonSave;
	private JButton buttonImportSaved;
	private JLabel labelTurnMax;
	private JSlider sliderTurnPerSecond;
	private JComboBox<ItemComBoBox> comboBoxLayout;
	private JComboBox<ItemComBoBox> comboBoxNiveau;
	private JComboBox<String> fileSaved;
	private JFileChooser fc;
	boolean isDone;
	private String niveau;
	private String fileSauvagarder;
	
	/**
	 * Constructeur de la ViewCommand
	 * @param controller
	 * @param game
	 */
	public ViewCommand(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		super(controller, game);
		fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		fc.setDialogTitle("Multiple file selection:");
		fc.setMultiSelectionEnabled(true);
		this.isDone = false;
	}
	
	@Override
	protected void createFrame() {
		// General informations
		this.setTitle("Game");
		this.setSize(new Dimension(800, 300));
		Dimension windowSize = this.getSize();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point centerPoint = ge.getCenterPoint();
		int dx = centerPoint.x - windowSize.width / 2 ;
		int dy = centerPoint.y - windowSize.height / 2 - 350;
		this.setLocation(dx, dy);
				
		// les icones
		String folderIcons = "icones/";	
		Icon icon_restart = new ImageIcon(folderIcons + "icon_restart.png");		
		Icon icon_run = new ImageIcon(folderIcons + "icon_run.png");
		Icon icon_step = new ImageIcon(folderIcons + "icon_step.png");
		Icon icon_pause = new ImageIcon(folderIcons + "icon_pause.png");
		Icon icon_save = new ImageIcon(folderIcons + "icon_save.png");
		Icon icon_solve = new ImageIcon(folderIcons + "icon_solve.png");
		
		// Buttons
		JPanel panelBtn = new JPanel(new GridLayout(1, 5));
		buttonStart = new JButton(icon_restart);
		buttonRun = new JButton(icon_run);
		buttonStep = new JButton(icon_step);
		buttonStop = new JButton(icon_pause);
		buttonSolve = new JButton(icon_solve);
		buttonSave = new JButton(icon_save);
		buttonImportSaved = new JButton("Import");
		
		panelBtn.add(buttonStart);
		panelBtn.add(buttonRun);
		panelBtn.add(buttonStep);
		panelBtn.add(buttonStop);	
		panelBtn.add(buttonSolve);
		panelBtn.add(buttonSave);
		
		// Slider	
		sliderTurnPerSecond = new JSlider(0, 10, 0);
		
		// Slider - paint the ticks and tarcks 
		sliderTurnPerSecond.setPaintTrack(true); 
		sliderTurnPerSecond.setPaintTicks(true); 
		sliderTurnPerSecond.setPaintLabels(true); 
  
        // Slider - set spacing 
		sliderTurnPerSecond.setMajorTickSpacing(1); 
		sliderTurnPerSecond.setMinorTickSpacing(1); 
        
        JPanel panelSliderTitle = new JPanel(new GridLayout(2, 1));
        JLabel title = new JLabel("Number of turns per second", JLabel.CENTER);
        
        panelSliderTitle.add(title);
        panelSliderTitle.add(sliderTurnPerSecond);
        
        JPanel panelSliderTurn = new JPanel(new GridLayout(1, 2));
                
        panelSliderTurn.add(panelSliderTitle);
        
        JPanel panelTurnFileCooser = new JPanel(new GridLayout(2, 1));
        
        this.labelTurnMax = new JLabel("Turn : "+this.game.getMaxturn(), JLabel.CENTER);
        
        panelTurnFileCooser.add(labelTurnMax);
        
        JPanel panelBtnMenu = new JPanel(new GridLayout(2, 3));
        JPanel choixLayout = new JPanel(new GridLayout(1, 2));
        JPanel choixSave = new JPanel(new GridLayout(1, 3));
        
        buttonChoose = new JButton("Choisir un Layout");
        
        comboBoxLayout = new JComboBox<ItemComBoBox>();
        
        comboBoxNiveau = new JComboBox<ItemComBoBox>();
        comboBoxNiveau.addItem(new ItemComBoBox("", "Choisir niveau"));
        comboBoxNiveau.addItem(new ItemComBoBox("niveau1", "Niveau 1"));
        comboBoxNiveau.addItem(new ItemComBoBox("niveau2", "Niveau 2"));
        comboBoxNiveau.addItem(new ItemComBoBox("niveau3", "Niveau 3"));
        
        fileSaved = new JComboBox<String>();
        
        comboBoxNiveau.setSelectedIndex(0);        
        
        choixLayout.add(buttonChoose);
        choixLayout.add(comboBoxLayout);
        choixSave.add(comboBoxNiveau);
        choixSave.add(fileSaved);
        choixSave.add(buttonImportSaved);
        
        panelBtnMenu.add(choixLayout);
        panelBtnMenu.add(choixSave);
        
        panelTurnFileCooser.add(labelTurnMax);
        panelTurnFileCooser.add(panelBtnMenu);
        panelSliderTurn.add(panelTurnFileCooser);

		
		JPanel panelprincipal = new JPanel(new GridLayout(2, 1));
		
		panelprincipal.add(panelBtn);
		panelprincipal.add(panelSliderTurn);
						
		this.setContentPane(panelprincipal);
		
		this.setVisible(true);
		
		//Définition des boutons
		this.buttonChoose.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				int returnValue = fc.showOpenDialog(null);
				
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File[] selectedFiles = fc.getSelectedFiles();				
					
					Arrays.asList(selectedFiles).forEach(x -> {
						if (x.isFile()) {
							comboBoxLayout.addItem(new ItemComBoBox(x.getAbsolutePath(), x.getName()));
						}
					});
					
					//On active le comboBox
					comboBoxLayout.setEnabled(true);
				}
			}
		});
		
		this.buttonStart.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.start();
			}
		});
		
		this.buttonSave.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{			
				controller.save();
			}
		});
		
		this.buttonRun.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.run(false);
			}
		});
		
		this.buttonStep.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.step(false);
			}
		});
		
		this.buttonStop.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.stop();
			}
		});
		
		this.buttonSolve.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.run(true);
			}
		});
		
		this.buttonImportSaved.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				((ControllerBombermanGame)controller).chargerPlateauSauvegarder(niveau,fileSauvagarder);
			}
		});		
		
		this.sliderTurnPerSecond.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent arg0) 
			{
				controller.setTime(sliderTurnPerSecond.getValue());
			}
		});
		
		this.comboBoxLayout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				ItemComBoBox itemComBoBox = (ItemComBoBox)comboBoxLayout.getSelectedItem();
				((ControllerBombermanGame)controller).loadMap(itemComBoBox.getChemin());
			}
		});
		
		
		this.fileSaved.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				fileSauvagarder = (String)fileSaved.getSelectedItem();				
			}
		});
		
		this.comboBoxNiveau.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				
				ItemComBoBox itemComBoBox = (ItemComBoBox)comboBoxNiveau.getSelectedItem();
				niveau = itemComBoBox.getChemin();
				switch (niveau) {
				case "niveau1":
					niveauFiles("./Sauvegarde/"+itemComBoBox.getChemin());
					break;
				case "niveau2":
					niveauFiles("./Sauvegarde/"+itemComBoBox.getChemin());
					break;
				case "niveau3":
					niveauFiles("./Sauvegarde/"+itemComBoBox.getChemin());
					break;	
				default:
					System.out.println("Le niveau selectionner n'existe pas");
					break;
				}
//				((ControllerBombermanGame)controller).loadMap(itemComBoBox.getChemin());
			}
		});
	}
	
	@Override
	public void update(Observable o, Object arg) {
		int turns = this.game.getMaxturn() - this.game.getTurn();
		this.labelTurnMax.setText("" + turns);
		//On affiche gameover
		boolean isDone = (Boolean)arg;
		if(isDone){
			((ControllerBombermanGame)controller).dispalyGameOver();
		}
	}
	
	private void niveauFiles(String path) {		
		File repertoire = new File(path);
        String liste[] = repertoire.list();      
        fileSaved.removeAllItems();
        if (liste != null) {         
            for (int i = 0; i < liste.length; i++) {
            	fileSaved.addItem(liste[i]);
            }
        } else {
            System.err.println("Nom de repertoire invalide");
        }
	}

	/**
	 * @return the buttonStart
	 */
	public JButton getButtonStart() {
		return buttonStart;
	}

	/**
	 * @param buttonStart the buttonStart to set
	 */
	public void setButtonStart(JButton buttonStart) {
		this.buttonStart = buttonStart;
	}

	/**
	 * @return the buttonRun
	 */
	public JButton getButtonRun() {
		return buttonRun;
	}

	/**
	 * @param buttonRun the buttonRun to set
	 */
	public void setButtonRun(JButton buttonRun) {
		this.buttonRun = buttonRun;
	}

	/**
	 * @return the buttonStep
	 */
	public JButton getButtonStep() {
		return buttonStep;
	}

	/**
	 * @param buttonStep the buttonStep to set
	 */
	public void setButtonStep(JButton buttonStep) {
		this.buttonStep = buttonStep;
	}

	/**
	 * @return the buttonStop
	 */
	public JButton getButtonStop() {
		return buttonStop;
	}

	/**
	 * @param buttonStop the buttonStop to set
	 */
	public void setButtonStop(JButton buttonStop) {
		this.buttonStop = buttonStop;
	}

	/**
	 * @return the buttonSolve
	 */
	public JButton getButtonSolve() {
		return buttonSolve;
	}

	/**
	 * @param buttonSolve the buttonSolve to set
	 */
	public void setButtonSolve(JButton buttonSolve) {
		this.buttonSolve = buttonSolve;
	}

	/**
	 * @return the buttonChoose
	 */
	public JButton getButtonChoose() {
		return buttonChoose;
	}

	/**
	 * @param buttonChoose the buttonChoose to set
	 */
	public void setButtonChoose(JButton buttonChoose) {
		this.buttonChoose = buttonChoose;
	}

	/**
	 * @return the buttonSave
	 */
	public JButton getButtonSave() {
		return buttonSave;
	}

	/**
	 * @param buttonSave the buttonSave to set
	 */
	public void setButtonSave(JButton buttonSave) {
		this.buttonSave = buttonSave;
	}

	/**
	 * @return the buttonImportSaved
	 */
	public JButton getButtonImportSaved() {
		return buttonImportSaved;
	}

	/**
	 * @param buttonImportSaved the buttonImportSaved to set
	 */
	public void setButtonImportSaved(JButton buttonImportSaved) {
		this.buttonImportSaved = buttonImportSaved;
	}

	/**
	 * @return the labelTurnMax
	 */
	public JLabel getLabelTurnMax() {
		return labelTurnMax;
	}

	/**
	 * @param labelTurnMax the labelTurnMax to set
	 */
	public void setLabelTurnMax(JLabel labelTurnMax) {
		this.labelTurnMax = labelTurnMax;
	}

	/**
	 * @return the sliderTurnPerSecond
	 */
	public JSlider getSliderTurnPerSecond() {
		return sliderTurnPerSecond;
	}

	/**
	 * @param sliderTurnPerSecond the sliderTurnPerSecond to set
	 */
	public void setSliderTurnPerSecond(JSlider sliderTurnPerSecond) {
		this.sliderTurnPerSecond = sliderTurnPerSecond;
	}

	/**
	 * @return the comboBoxLayout
	 */
	public JComboBox<ItemComBoBox> getComboBoxLayout() {
		return comboBoxLayout;
	}

	/**
	 * @param comboBoxLayout the comboBoxLayout to set
	 */
	public void setComboBoxLayout(JComboBox<ItemComBoBox> comboBoxLayout) {
		this.comboBoxLayout = comboBoxLayout;
	}

	/**
	 * @return the comboBoxNiveau
	 */
	public JComboBox<ItemComBoBox> getComboBoxNiveau() {
		return comboBoxNiveau;
	}

	/**
	 * @param comboBoxNiveau the comboBoxNiveau to set
	 */
	public void setComboBoxNiveau(JComboBox<ItemComBoBox> comboBoxNiveau) {
		this.comboBoxNiveau = comboBoxNiveau;
	}

	/**
	 * @return the fileSaved
	 */
	public JComboBox<String> getFileSaved() {
		return fileSaved;
	}

	/**
	 * @param fileSaved the fileSaved to set
	 */
	public void setFileSaved(JComboBox<String> fileSaved) {
		this.fileSaved = fileSaved;
	}

	/**
	 * @return the fc
	 */
	public JFileChooser getFc() {
		return fc;
	}

	/**
	 * @param fc the fc to set
	 */
	public void setFc(JFileChooser fc) {
		this.fc = fc;
	}

	/**
	 * @return the isDone
	 */
	public boolean isDone() {
		return isDone;
	}

	/**
	 * @param isDone the isDone to set
	 */
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	/**
	 * @return the niveau
	 */
	public String getNiveau() {
		return niveau;
	}

	/**
	 * @param niveau the niveau to set
	 */
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	/**
	 * @return the fileSauvagarder
	 */
	public String getFileSauvagarder() {
		return fileSauvagarder;
	}

	/**
	 * @param fileSauvagarder the fileSauvagarder to set
	 */
	public void setFileSauvagarder(String fileSauvagarder) {
		this.fileSauvagarder = fileSauvagarder;
	}
}