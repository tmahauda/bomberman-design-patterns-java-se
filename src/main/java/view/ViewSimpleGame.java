package view;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Label;
import java.awt.Point;
import java.util.Observable;
import controller.Controller;
import ia.solve.Solveable;
import model.Game;

/**
 * Vue test
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ViewSimpleGame extends View {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Label qui permet d'afficher le nombre de tours courant
	 */
	private Label labelTour;

	/**
	 * Constructeur de la ViewSimpleGame
	 * @param controller
	 * @param game
	 */
	public ViewSimpleGame(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		super(controller, game);
	}

	@Override
	protected void createFrame() {
		this.setTitle("Game");
		this.setSize(new Dimension(700, 700));
		Dimension windowSize = this.getSize();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point centerPoint = ge.getCenterPoint();
		int dx = centerPoint.x - windowSize.width / 2 ;
		int dy = centerPoint.y - windowSize.height / 2 - 350;
		this.setLocation(dx, dy);
		this.setVisible(true);
		
		this.labelTour = new Label();
		this.getContentPane().add(this.labelTour);
		this.update(this.game, null);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		this.labelTour.setText("Nombre de tour : " + this.game.getTurn());
	}

	/**
	 * @return the labelTour
	 */
	public Label getLabelTour() {
		return labelTour;
	}

	/**
	 * @param labelTour the labelTour to set
	 */
	public void setLabelTour(Label labelTour) {
		this.labelTour = labelTour;
	}
	
}
