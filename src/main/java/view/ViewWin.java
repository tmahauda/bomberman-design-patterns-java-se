package view;

import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.Controller;
import controller.bomberman.ControllerBombermanGame;
import ia.solve.Solveable;
import model.Game;
import model.bomberman.map.Map;
import view.bomberman.ViewBomberman;

/**
 * Vue qui permet de passer au niveau suivant
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ViewWin extends View {

	private static final long serialVersionUID = 1L;

	public ViewWin(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		super(controller, game);
	}

	@Override
	public void update(Observable o, Object arg) {
		
	}

	@Override
	protected void createFrame() {
		Object[] options = {"Next Level",
        "Quitter"};		
		
		JFrame parent = new JFrame();
		
		int n = JOptionPane.showOptionDialog(parent,
				"Félicitations vous avez gagné",
				"You win",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE,
				null,     
				options,  
				options[0]);

		//Win. Passe au niveau suivant			
		if (n == 0) {
			Map.getInstance().getViewBomberman().dispose();			
			
			String path = Map.getInstance().getFilename();			
			
			String path2 = path.substring(path.length() - 5, path.length() - 4);
			int niveau = Integer.parseInt(path2);
			niveau++;			
			String pathSansNiveau = path.substring(0, path.length()-5);
			
			switch (niveau) {
			case 1:
				Map.getInstance().setFilename(pathSansNiveau+"1.lay");
				try {
					Map.getInstance().load();
//					ViewBomberman vm = new ViewBomberman(controller, game);
					this.controller.start();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 2:
				Map.getInstance().setFilename(pathSansNiveau+"2.lay");
				try {
					Map.getInstance().load();
//					ViewBomberman vm = new ViewBomberman(controller, game);
					this.controller.start();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;	
			
			case 3:
				Map.getInstance().setFilename(pathSansNiveau+"3.lay");
				try {
					Map.getInstance().load();
//					ViewBomberman vm = new ViewBomberman(controller, game);
					this.controller.start();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;	
			case 4 :
				this.controller.init();
				Object[] optionsSansSuivant = {"Quitter"};		
				
				JFrame frameSansSuivant = new JFrame();
				
				int d = JOptionPane.showOptionDialog(frameSansSuivant,
						"Félicitations vous avez fini le jeu",
						"You win",
						JOptionPane.OK_OPTION,
						JOptionPane.INFORMATION_MESSAGE,
						null,     
						optionsSansSuivant,  
						optionsSansSuivant[0]);
				
				
				
				break;
			default:
				Object[] optionsFin = {"OK"};	
				JFrame frameFin = new JFrame();
				
				int m = JOptionPane.showOptionDialog(frameFin,
						"Le niveau n'existe pas",
						"Bomberman",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.INFORMATION_MESSAGE,
						null,     
						optionsFin,  
						optionsFin[0]);
				System.out.println("Niveau n'existe pas");
				break;
			}
			
//			this.map.load("/home/etudiant/Bureau/layouts/niveau2.lay");
		}else {
			System.exit(0);
		}

		
	}

}
