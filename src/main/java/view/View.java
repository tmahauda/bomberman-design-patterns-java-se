package view;

import java.util.Observer;
import javax.swing.JFrame;
import controller.Controller;
import ia.solve.Solveable;
import model.Game;

/**
 * Vue abstraite qui joue le rôle d'observateur permettant de mettre à jour en temps réel les modifications
 * faites dans le modèle Game dans les composants graphiques.
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class View extends JFrame implements Observer {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Controlleur qui gère les actions de l'utilisateur
	 */
	protected Controller<? extends Solveable> controller;
	
	/**
	 * Modèle jeu qui contient les données à afficher dans les composants graphiques
	 */
	protected Game<? extends Solveable> game;
	
	/**
	 * Constructeur de la vue
	 * @param controller
	 * @param game
	 */
	public View(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		this.controller = controller;
		this.game = game;
		this.game.addObserver(this);
		this.createFrame();
	}
	
	/**
	 * Méthode qui permet de créer la vue en disposant les composants graphiques
	 * (boutons, label, etc.)
	 */
	protected abstract void createFrame();
}
