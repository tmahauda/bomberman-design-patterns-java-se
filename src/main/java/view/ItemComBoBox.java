package view;

/**
 * Class qui permet de personnalisé les elements du ComBoBox
 * chaque element contien un nom et son chemin
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ItemComBoBox {
	
	private String chemin;
	private String name;
		
	public ItemComBoBox(String chemin, String name) {		
		this.chemin = chemin;
		this.name = name;
	}

	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
        return name;
    }
}