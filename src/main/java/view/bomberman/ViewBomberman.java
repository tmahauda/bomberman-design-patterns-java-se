package view.bomberman;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import controller.Controller;
import controller.bomberman.ControllerBombermanGame;
import controller.bomberman.actionnable.AgentAction;
import controller.bomberman.itemable.ItemType;
import controller.bomberman.moveable.AgentMove;
import ia.solve.Solveable;
import model.Game;
import model.bomberman.map.Map;
import view.View;

/**
 * Vue qui affiche le plateau de bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ViewBomberman extends View implements KeyListener {
	
	private static final long serialVersionUID = 1L;
	private PanelBomberman panelBomberman;
	
	private JLabel lifeValeur;
	private JLabel fireValeur;
	private JLabel bombValeur;
	private JLabel speedValeur;
	private JLabel pointValeur;

	public ViewBomberman(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		super(controller, game);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		((ControllerBombermanGame)controller).doMove(AgentMove.STOP);	
		updateCaracteristiqueBomberman();
		this.repaint();
	}
	
	private void updateCaracteristiqueBomberman() {	
		lifeValeur.setText(""+Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.LIFE).getCapacityActualItem());
		fireValeur.setText(""+Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.FIRE).getCapacityActualItem());
		bombValeur.setText(""+Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.BOMB).getCapacityActualItem());
		speedValeur.setText(""+Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.SPEED).getCapacityActualItem());
		pointValeur.setText(""+Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.POINT).getCapacityActualItem());
	}
	
	@Override
	protected void createFrame() {
		//La fenetre
		this.setTitle("Bomberman");
		this.setSize(new Dimension(1280, 720));
		Dimension windowSize = this.getSize();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point centerPoint = ge.getCenterPoint();
		int dx = centerPoint.x - windowSize.width / 2 ;
		int dy = centerPoint.y - windowSize.height / 2 - 640;
		this.setLocation(dx, dy);
		
		//Le plateau
		this.panelBomberman = new PanelBomberman();
		
		JPanel base = new JPanel();		
		base.setLayout(new BorderLayout(1280, 740));
		
		JPanel information = new JPanel();
		information.setPreferredSize(new Dimension(getSize().width, 20));
		
		JLabel life = new JLabel("Life : ");
		lifeValeur = new JLabel();
		
		ImageIcon icon = new ImageIcon("./image/piques.png");		
		JLabel piques1 = new JLabel(icon);
		JLabel piques2 = new JLabel(icon);
		JLabel piques3 = new JLabel(icon);
		JLabel piques4 = new JLabel(icon);
		
		JLabel fire = new JLabel("Fire : ");
		fireValeur = new JLabel();		
		
		JLabel bomb = new JLabel("Bomb : ");
		bombValeur = new JLabel();
		
		JLabel speed = new JLabel("Speed : ");
		speedValeur = new JLabel();
		
		JLabel attack = new JLabel("Point : ");
		pointValeur = new JLabel();
		
		
		updateCaracteristiqueBomberman();
		information.add(life);		
		information.add(lifeValeur);
		information.add(piques1);
		information.add(fire);
		information.add(fireValeur);
		information.add(piques2);
		information.add(bomb);
		information.add(bombValeur);
		information.add(piques3);
		information.add(speed);
		information.add(speedValeur);
		information.add(piques4);
		information.add(attack);
		information.add(pointValeur);
		
		panelBomberman.setPreferredSize(new Dimension(windowSize.width, windowSize.height));
		base.add(information, BorderLayout.NORTH);
		base.add(panelBomberman, BorderLayout.SOUTH);		
		
		this.setContentPane(base);
		this.setVisible(true);
		this.addKeyListener(this);
		
		//bomberman
		
		//Lorsqu'on quitte le plateau on remet la commande à l'état d'initiale
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
                controller.init();
            }
		});
	}
	

	/**
	 * Effectuer les déplacement sur les bomberman
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_UP:						
				((ControllerBombermanGame)controller).doMove(AgentMove.MOVE_UP);
				break;
			case KeyEvent.VK_DOWN:				
				((ControllerBombermanGame)controller).doMove(AgentMove.MOVE_DOWN);
				break;
			case KeyEvent.VK_LEFT:				
				((ControllerBombermanGame)controller).doMove(AgentMove.MOVE_LEFT);
				break;
			case KeyEvent.VK_RIGHT:				
				((ControllerBombermanGame)controller).doMove(AgentMove.MOVE_RIGHT);
				break;
			default:
				((ControllerBombermanGame)controller).doMove(AgentMove.STOP);
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	/**
	 * Effectuer les actions que peut réaliser le bomberman
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		switch(e.getKeyChar()) {
			//Poser une bombe
			case 'b':
				((ControllerBombermanGame)controller).doAction(AgentAction.PUT_BOMB);
				break;
			default:
		}
	}
}
