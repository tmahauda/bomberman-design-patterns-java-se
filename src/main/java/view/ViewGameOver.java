package view;

import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.Controller;
import ia.solve.Solveable;
import model.Game;
import model.bomberman.map.Map;

/**
 * Vue qui affiche gameOver
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class ViewGameOver extends View {

	private static final long serialVersionUID = 1L;

	public ViewGameOver(Controller<? extends Solveable> controller, Game<? extends Solveable> game) {
		super(controller, game);
	}

	@Override
	public void update(Observable arg0, Object arg1) {

	}

	@Override
	protected void createFrame() {
		JFrame parent = new JFrame();
	    
	    Object[] options = {"OK"};
	    int n = JOptionPane.showOptionDialog(parent,
	    	"Game Over"
	    	,"You lose",
            JOptionPane.PLAIN_MESSAGE,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);
 
	    
	    if (n == 0) {
	    	Map.getInstance().getViewBomberman().dispose();
		}
		
		
	}

}
