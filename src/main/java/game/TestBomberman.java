package game;

import controller.bomberman.ControllerBombermanGame;
import model.bomberman.BombermanGame;
import model.bomberman.FactoryBombermanGame;

/**
 * Classe qui teste l'implémentation du jeu Bomberman en instanciant le modele BombermanGame 
 * et le controlleur ControllerBomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class TestBomberman {
	
	public static void main(String[] args) {
		try {			
			BombermanGame bombermanGame = FactoryBombermanGame.getBombermanGameForPlayer(100, 1000);
			ControllerBombermanGame controller = new ControllerBombermanGame(bombermanGame);
			controller.init();
		} catch (Exception e) {
			//Affiche une pop-up erreur pour l'utilisateur
		}
	}
}
