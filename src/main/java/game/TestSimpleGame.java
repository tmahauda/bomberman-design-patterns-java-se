package game;

import controller.ControllerSimpleGame;
import model.SimpleGame;

/**
 * Classe qui teste l'implémentation du jeu SimpleGame en instanciant le modele SimpleGame et le controlleur ControllerSimpleGame
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class TestSimpleGame {
	
	public static void main(String[] args) {
		SimpleGame simpleGame = new SimpleGame(5, 1000);
		ControllerSimpleGame controller = new ControllerSimpleGame(simpleGame);
		controller.init();
	}
}
