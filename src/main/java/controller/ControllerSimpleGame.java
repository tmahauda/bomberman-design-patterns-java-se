package controller;

import model.Game;
import view.ViewCommand;
import view.ViewSimpleGame;

/**
 * Contrôleur qui permet de contrôler le jeu quand des actions ont été effectuées par
 * l’utilisateur dans la Vue ViewSimpleGame et ViewGame (par exemple lorsque l’utilisateur clique sur un bouton)
 * et mettre à jour le modèle Game en conséquence.
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 *
 */
public class ControllerSimpleGame extends Controller {
	
	/**
	 * Vue jeu test
	 */
	private ViewSimpleGame vsg;
	
	/**
	 * Vue commande
	 */
	private ViewCommand vc;
	
	@SuppressWarnings("unchecked")
	public ControllerSimpleGame(Game<?> game) {
		super(game);
		this.vsg = new ViewSimpleGame(this, game);
		this.vc = new ViewCommand(this, game);
	}

	@Override
	public void init() {
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(false);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(false);		
	}
	
	@Override
	public void start() {
		this.game.init();
		
		//On active les boutons run le slider
		//Les autres éléments sont désactivés
		this.vsg.update(this.game, null);
		this.vc.getButtonStart().setEnabled(false);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
	}
	
	@Override
	public void startSoluce() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void step(boolean soluce) {
		this.game.step(soluce);
		
		//Seul le bouton stop est désactivé
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(true);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
	}

	@Override
	public void run(boolean soluce) {
		this.game.launch(soluce);
		
		//Seul le bouton stop et slider sont activés
		this.vc.getButtonStart().setEnabled(false);
		this.vc.getButtonRun().setEnabled(false);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(true);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
	}

	@Override
	public void stop() {
		this.game.stop();
		
		//On active tous les boutons sauf stop
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(true);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
	}

	@Override
	public void setTime(long time) {
		this.game.setTime(time*1000);
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}
}
