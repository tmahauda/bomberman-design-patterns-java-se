package controller;

import ia.solve.Solveable;
import model.Game;

/**
 * Interface qui permet d'assurer les contrôles du jeu quand des actions ont été effectuées par
 * l’utilisateur dans la Vue (par exemple lorsque l’utilisateur clique sur un bouton)
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 *
 */
public abstract class Controller<T extends Solveable> {
	
	/**
	 * Le jeu à controller
	 */
	protected Game<T> game;
	
	public Controller(Game<T> game) {
		this.game = game;
	}
	
	/**
	 * Méthode qui initialise la vue
	 */
	abstract public void init();
	
	/**
	 * Méthode qui initialise la solution du jeu
	 */
	abstract public void startSoluce();
	
	/**
	 * Méthode qui initialise le jeu
	 */
	abstract public void start();
	
	/**
	 * Méthode qui effectue un tour dans le jeu
	 */
	abstract public void step(boolean soluce);
	
	/**
	 * Méthode qui démarre le jeu tant qu'il y a des tours à effectuer
	 */
	abstract public void run(boolean soluce);
	
	/**
	 * Méthode qui met en pause le jeu
	 */
	abstract public void stop();
	
	/**
	 * Méthode qui modifie le temps d'execution entre chaque tour de jeu.
	 */
	abstract public void setTime(long time);
	
	/**
	 * Sauvegarder le jeu
	 */
	abstract public void save();
}
