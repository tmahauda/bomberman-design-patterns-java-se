package controller.bomberman.itemable.bonus.life;

import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.kind.AgentKind;

/**
 * Invincibilité (ou encore Fire Suit, FlameProof Jacket) - Permet au bomberman d'être totalement invincible pendant x tours. 
 * L'utilisateur de ce bonus clignote rapidement.
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentItemableBonusFireSuit extends AgentItemableBonusLife {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusFireSuit() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		return itemType == ItemType.FIRE_SUIT;
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		bomberman.setInvincible(true);
	}
}
