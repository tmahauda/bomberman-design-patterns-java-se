package controller.bomberman.itemable.bonus.life;

import java.io.Serializable;
import controller.bomberman.itemable.Itemable;

/**
 * Bonus de vie 
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class AgentItemableBonusLife implements Itemable, Serializable {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusLife() {
		
	}
}
