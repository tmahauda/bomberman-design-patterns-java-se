package controller.bomberman.itemable.bonus.sick;

import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.kind.AgentKind;

/**
 * Rend malade pendant x tours (flag isSick à activer pour l’affichage).
 * Lorsqu’il est malade l’agent ne peut pas poser de bombes.
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class AgentItemableBonusSickSkull extends AgentItemableBonusSick {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusSickSkull() {
		
	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		return itemType == ItemType.SKULL;
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		bomberman.setSick(true);
	}
}
