package controller.bomberman.itemable.bonus.bomb;

import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.CapacityItem;
import model.bomberman.agent.kind.AgentKind;

/**
 * Augmente ou diminue le nombre de pose simultané de bombe à chaque tour
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentItemableBonusBombPut extends AgentItemableBonusBomb {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombPut() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.BOMB);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case BOMB_DOWN:
				return true;
			case BOMB_UP:
				return true;
			case BOMB_FULL_DOWN:
				return true;
			case BOMB_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.BOMB);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case BOMB_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case BOMB_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case BOMB_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case BOMB_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}

}
