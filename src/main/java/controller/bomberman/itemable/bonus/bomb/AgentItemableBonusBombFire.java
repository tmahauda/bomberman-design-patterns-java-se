package controller.bomberman.itemable.bonus.bomb;

import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.CapacityItem;
import model.bomberman.agent.kind.AgentKind;

/**
 * Augmente ou diminue la portée de l'explosion des bombes de x cases
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentItemableBonusBombFire extends AgentItemableBonusBomb {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombFire() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		if(bomberman == null) return false;
		if(itemType == null) return false;
		
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.FIRE);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case FIRE_DOWN:
				return true;
			case FIRE_UP:
				return true;
			case FIRE_FULL_DOWN:
				return true;
			case FIRE_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		if(bomberman == null) return;
		if(itemType == null) return;
		
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.FIRE);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case FIRE_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case FIRE_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case FIRE_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case FIRE_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}
}
