package controller.bomberman.itemable.bonus.bomb;

import java.io.Serializable;

import controller.bomberman.itemable.Itemable;

/**
 * Bonus sur les bombes
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class AgentItemableBonusBomb implements Itemable, Serializable {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBomb() {
	
	}
}
