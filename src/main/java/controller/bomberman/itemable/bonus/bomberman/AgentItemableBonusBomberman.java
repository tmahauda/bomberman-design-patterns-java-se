package controller.bomberman.itemable.bonus.bomberman;

import java.io.Serializable;

import controller.bomberman.itemable.Itemable;

/**
 * Bonus sur les caractéristiques du bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class AgentItemableBonusBomberman implements Itemable, Serializable {
	
	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBomberman() {

	}
}
