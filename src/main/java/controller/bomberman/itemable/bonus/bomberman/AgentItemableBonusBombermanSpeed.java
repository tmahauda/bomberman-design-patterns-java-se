package controller.bomberman.itemable.bonus.bomberman;

import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.CapacityItem;
import model.bomberman.agent.kind.AgentKind;

/**
 * Les patins à roulettes augmentent ou diminuent la vitesse du bomberman. 
 * Elle se traduit par un déplacement de x cases autorisé à chaque tour
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentItemableBonusBombermanSpeed extends AgentItemableBonusBomberman {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombermanSpeed() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.SPEED);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case SPEED_DOWN:
				return true;
			case SPEED_UP:
				return true;
			case SPEED_FULL_DOWN:
				return true;
			case SPEED_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.SPEED);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case SPEED_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case SPEED_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case SPEED_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case SPEED_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}

}
