package controller.bomberman.moveable;

/**
 * Enumération des déplacements autorisés sur le plateau
 * - Haut
 * - Bas
 * - Gauche
 * - Droite
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public enum AgentMove {
	MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, STOP;
}
