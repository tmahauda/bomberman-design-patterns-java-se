package controller.bomberman.moveable;

import controller.bomberman.moveable.AgentMoveableStill;
import model.bomberman.agent.Agent;

/**
 * Un agent reste immobile
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentMoveableStill extends AgentMoveable {

	private static final long serialVersionUID = 1L;

	public AgentMoveableStill() {

	}
	
	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {
		return true;
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		agent.setAgentMove(AgentMove.STOP);
	}
}
