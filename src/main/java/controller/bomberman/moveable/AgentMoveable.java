package controller.bomberman.moveable;

import java.io.Serializable;

import controller.bomberman.moveable.AgentMoveable;
import model.bomberman.agent.Agent;
import model.bomberman.map.Map;

/**
 * Déplacement commun à tous les agents
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentMoveable implements Moveable, Serializable {

	private static final long serialVersionUID = 1L;

	public AgentMoveable() {
		
	}
	
	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {
		if(agent == null) return false;
		if(move == null) return false;
		
		int x = agent.getX();
		int y = agent.getY();
		
		//L'agent ne doit pas recontrer un mur et un mur cassable
		switch(move) {
			case MOVE_LEFT:
				return !Map.getInstance().getWalls()[x-1][y] && !Map.getInstance().getStart_brokable_walls()[x-1][y];
			case MOVE_RIGHT:
				return !Map.getInstance().getWalls()[x+1][y] && !Map.getInstance().getStart_brokable_walls()[x+1][y];
			case MOVE_UP:
				return !Map.getInstance().getWalls()[x][y-1] && !Map.getInstance().getStart_brokable_walls()[x][y-1];
			case MOVE_DOWN:
				return !Map.getInstance().getWalls()[x][y+1] && !Map.getInstance().getStart_brokable_walls()[x][y+1];
			default:
				return false;
		}
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		if(agent == null) return;
		if(move == null) return;
		
		int x = agent.getX();
		int y = agent.getY();
		
		switch(move) {
			case MOVE_LEFT:
				agent.setX(x-1);
				agent.setAgentMove(move);
				break;
			case MOVE_RIGHT:
				agent.setX(x+1);
				agent.setAgentMove(move);
				break;
			case MOVE_UP:
				agent.setY(y-1);
				agent.setAgentMove(move);
				break;
			case MOVE_DOWN:
				agent.setY(y+1);
				agent.setAgentMove(move);
				break;
			default:
				return;
		}
	}
}
