package controller.bomberman.moveable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import controller.bomberman.moveable.AgentMoveableChase;
import model.bomberman.agent.Agent;

/**
 * Poursuivre un agent bomberman
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentMoveableChase extends AgentMoveable {

	private static final long serialVersionUID = 1L;

	public AgentMoveableChase() {

	}
	
	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {		
		return true;		
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		//L'agent poursuit le bomberman
		if(agent == null) return;
		if(move == null) return;
		
		int xAgent = agent.getX();
		int yAgent = agent.getY();
		
		int xKind = model.bomberman.map.Map.getInstance().getCurrentAgentKind().getX();
		int yKind = model.bomberman.map.Map.getInstance().getCurrentAgentKind().getY();
		
//		System.out.println("racine : " 		+Math.sqrt(Math.pow(xAgent-xKind, 2) + Math.pow(yAgent-yKind, 2)));
//		System.out.println("racine left : "	+Math.sqrt(Math.pow(xAgent-1-xKind, 2) + Math.pow(yAgent-yKind, 2)));
//		System.out.println("racine right: "	+Math.sqrt(Math.pow(xAgent+1-xKind, 2) + Math.pow(yAgent-yKind, 2)));
//		System.out.println("racine up: "	+Math.sqrt(Math.pow(xAgent-xKind, 2) + Math.pow(yAgent-1-yKind, 2)));
//		System.out.println("racine down: "	+Math.sqrt(Math.pow(xAgent-xKind, 2) + Math.pow(yAgent+1-yKind, 2)));
		
		// une collection Map qui contient comme clés les movement(LEFT, RIGHT, UP, DOWN) 
		//et comme valeur la distance entre le Rajion -en appliquant la nouvelle emplacement- et le bomberman 
		final java.util.Map<String, Double> moveListe = new HashMap<>();
		moveListe.put("MOVE_LEFT", Math.sqrt(Math.pow(xAgent-1-xKind, 2) + Math.pow(yAgent-yKind, 2)));
		moveListe.put("MOVE_RIGHT", Math.sqrt(Math.pow(xAgent+1-xKind, 2) + Math.pow(yAgent-yKind, 2)));
		moveListe.put("MOVE_UP", Math.sqrt(Math.pow(xAgent-xKind, 2) + Math.pow(yAgent-1-yKind, 2)));
		moveListe.put("MOVE_DOWN", Math.sqrt(Math.pow(xAgent-xKind, 2) + Math.pow(yAgent+1-yKind, 2)));
		
		// en trier la collection par valeur
		final Map<String, Double> moveListeTrier = moveListe.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		
		// Obtenir la liste des clés
		Set<String> listKeys = moveListeTrier.keySet();  
		Iterator<String> iterateur = listKeys.iterator();

		// Parcourir les clés
		// et verifier si il y a un mur avant d'appliquer le movement
		// loop1 un nom qui nous permet de sortir de la boucle while
		loop1:while(iterateur.hasNext())
		{
			String key= (String)iterateur.next();
//			System.out.println (key + "=>" + moveListeTrier.get(key));
			
			switch(key) {
				case "MOVE_LEFT":									
					if(!model.bomberman.map.Map.getInstance().getStart_brokable_walls()[xAgent-1][yAgent]) {
						agent.setX(xAgent-1);
						break loop1;
					}
					break;
				case "MOVE_RIGHT":									
					if(!model.bomberman.map.Map.getInstance().getStart_brokable_walls()[xAgent+1][yAgent]) {
						agent.setX(xAgent+1);
						break loop1;
					}
					break;
				case "MOVE_UP":									
					if(!model.bomberman.map.Map.getInstance().getStart_brokable_walls()[xAgent][yAgent-1]) {
						agent.setY(yAgent-1);
						break loop1;
					}		
					break;
				case "MOVE_DOWN":									
					if(!model.bomberman.map.Map.getInstance().getStart_brokable_walls()[xAgent][yAgent+1]) {					
						agent.setY(yAgent+1);
						break loop1;
					}	
					break;
				default:
					System.out.println("pas de mouvement IS LEGAL");					
			}				
		}										
	}
}
