package controller.bomberman.actionnable;

/**
 * Enumération des états d'une bombe avant d'exploser
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public enum StateBomb {
	Step1,Step2,Step3,Boom
}