package controller.bomberman.actionnable;
  
import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.Agent;
import model.bomberman.agent.CapacityItem;
import model.bomberman.map.Map;

/**
 * Attaquer un autre agent. En l'occurence l'actuel bomberman manipulé dans le plateau
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentActionnableAttack extends AgentActionnable {

	private static final long serialVersionUID = 1L;

	public AgentActionnableAttack() {

	}
	
	@Override
	public boolean isLegalAction(Agent agent, AgentAction action) {
		//Il peut attaquer s'il se trouve sur la même coordonnée
		
		//On récupère les coordonnées de l'ennemi
		int ennemyX = agent.getX();
		int ennemyY = agent.getY();
		
		//On récupère les coordonnées du bomberman
		int kindX = Map.getInstance().getCurrentAgentKind().getX();
		int kindY = Map.getInstance().getCurrentAgentKind().getY();
		
		//Si l'ennemi est sur le bomberman
		return action == AgentAction.ATTACK && ennemyX == kindX && ennemyY == kindY;
	}

	@Override
	public void doAction(Agent agent, AgentAction action) {
		//On récupère le dégat de l'ennemi
		CapacityItem attack = agent.getCapacityItem(ItemType.ATTACK);
		if(attack == null) return;
		
		int degat = attack.getCapacityActualItem();
		
		//On applique le dégat sur l'agent bomberman
		CapacityItem life = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.LIFE);
		if(life == null) return;
		
		int saveStep = life.getStep();
		life.setStep(degat);
		life.decrementCapacityActualItem();
		life.setStep(saveStep);
	}
}
