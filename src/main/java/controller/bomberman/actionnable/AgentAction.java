package controller.bomberman.actionnable;

/**
 * Enumération des actions possibles que peut réaliser les agents
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public enum AgentAction {
	PUT_BOMB, ATTACK;
}
