package controller.bomberman.actionnable;

import model.bomberman.agent.Agent;

/**
 * Réaliser une action dans le jeu par un agent
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public interface Actionnable {
	
	/**
	 * Vérifier si une action est réalisable par un agent
	 * @param agent
	 * @param action
	 * @return vrai si on peut. Faux dans le cas contraire
	 */
	public boolean isLegalAction(Agent agent, AgentAction action);
	
	/**
	 * Effectuer une action par l'agent
	 * @param agent
	 * @param action
	 */
	public void doAction(Agent agent, AgentAction action);
}
