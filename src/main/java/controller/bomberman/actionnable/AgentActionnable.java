package controller.bomberman.actionnable;

import java.io.Serializable;

/**
 * Réalisation des actions communes à tous les agents
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class AgentActionnable implements Actionnable, Serializable {

	private static final long serialVersionUID = 1L;

	public AgentActionnable() {
		
	}
}
