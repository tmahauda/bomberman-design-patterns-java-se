package controller.bomberman.actionnable;
  
import controller.bomberman.itemable.ItemType;
import model.bomberman.agent.Agent;
import model.bomberman.agent.CapacityItem;
import model.bomberman.map.Map;

/**
 * Poser une bombe par un agent. En l'occurence le bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class AgentActionnablePutBomb extends AgentActionnable {

	private static final long serialVersionUID = 1L;

	public AgentActionnablePutBomb() {

	}
	
	@Override
	public boolean isLegalAction(Agent agentKind, AgentAction action) {
		CapacityItem nbCapacityBomb = agentKind.getCapacityItem(ItemType.BOMB);
		int nbPutBomb = Map.getInstance().getStart_Bombs().size();
		
		//L'action est bien de posé une bombe
		boolean isPutBomb = action == AgentAction.PUT_BOMB;
		
		//Vérifier la capacité qu'il peut poser simultanément le bomberman sur le plateau
		boolean isCapacityBomb = nbPutBomb <= nbCapacityBomb.getCapacityActualItem();
		
		//Vérifier si la bombe n'est pas déjà posé sur la case
		boolean isNotAlreadyPutBomb = true;
		for(InfoBomb bomb : Map.getInstance().getStart_Bombs()) {
			if(bomb.getX() == agentKind.getX() && bomb.getY() == agentKind.getY()) {
				isNotAlreadyPutBomb = false;
			}
		}
		
		//L'agent n'est pas malade
		
		return isPutBomb && isCapacityBomb && isNotAlreadyPutBomb;
	}

	@Override
	public void doAction(Agent agentKind, AgentAction action) {
		//On pose une bombe à la position courante du bomberman
		CapacityItem capacityFire = agentKind.getCapacityItem(ItemType.FIRE);
		InfoBomb bomb = new InfoBomb(agentKind.getX(), agentKind.getY(), capacityFire.getCapacityActualItem(), agentKind.getStateBomb());
		Map.getInstance().getStart_Bombs().add(bomb);
	}
}
