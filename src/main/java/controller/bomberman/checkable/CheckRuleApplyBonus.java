package controller.bomberman.checkable;

import java.util.Iterator;

import controller.bomberman.itemable.InfoItem;
import model.bomberman.agent.kind.AgentKind;
import model.bomberman.map.Map;

/**
 * Vérifier si un agent bomberman à récupérer un bonus.
 * Si oui on l'applique
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class CheckRuleApplyBonus extends CheckRule {

	private static final long serialVersionUID = 1L;

	public CheckRuleApplyBonus() {

	}

	@Override
	public boolean checkRule() {
		//Cette règle est éxecuté à chaque tour
		return true;
	}
	
	@Override
	public void executeRule() {
		for(AgentKind agent : Map.getInstance().getAgentsKind()) {
			this.applyBonus(agent);
		}
	}
	
	private void applyBonus(AgentKind agentKind) {
		//Pour chaque bonus placé sur le plateau
		for(Iterator<InfoItem> iteratorItem = Map.getInstance().getStart_Items().iterator(); iteratorItem.hasNext(); ) {
			
			InfoItem bonus = iteratorItem.next();
			
			//On récupère les coordonnées du bonus
			int bonusX = bonus.getX();
			int bonusY = bonus.getY();
			
			//On récupère les coordonnées du bomberman
			int bombermanX = agentKind.getX();
			int bombermanY = agentKind.getY();
			
			//Si le bomberman est sur un bonus
			if(bonusX == bombermanX && bonusY == bombermanY) {
				
				//On applique le bonus sur le bomberman
				agentKind.doBonus(bonus.getType());
				
				//Puis on supprime le bonus du plateau
				iteratorItem.remove();
			}
		}
	}
}
