package controller.bomberman.checkable;

import java.util.Random;
import controller.bomberman.itemable.InfoItem;
import controller.bomberman.itemable.ItemType;
import model.bomberman.map.Map;

/**
 * Placer des bonus aléatoirement sur le plateau
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class CheckRulePutBonusRandomly extends CheckRule {

	private static final long serialVersionUID = 1L;

	public CheckRulePutBonusRandomly() {

	}

	@Override
	public boolean checkRule() {
		//On execute la règle s'il n'y a plus de bonus
		return Map.getInstance().getStart_Items().isEmpty();
	}
	
	@Override
	public void executeRule() {
		//On place 10 bonus aléatoirement sur le plateau
		for(int i=0; i<10; i++) {
			
			ItemType bonus = this.choseRandomBonus();
			int x = this.choseRandomX();
			int y = this.choseRandomY();
			
			//Si il y a un mur ou un item déjà déplacé à cette coordonnée
			//on recommence le tirage
			//Sinon on place l'item
			while(!this.isPossibleToPutBonus(x, y)) {
				x = this.choseRandomX();
				y = this.choseRandomY();
			}
		
			InfoItem item = new InfoItem(x, y, bonus);
			Map.getInstance().getStart_Items().add(item);	
		}
	}

	/**
	 * Choisir un bonus aléatoirement
	 * @return le bonus tirée aléatoirement
	 */
	private ItemType choseRandomBonus() {
		int maxBonus = ItemType.valuesUse().size();
		int randomBonus = new Random().nextInt(maxBonus);
		
		return ItemType.valuesUse().get(randomBonus);
	}
	
	private int choseRandomX() {
		return new Random().nextInt(Map.getInstance().getSize_x());
	}
	
	private int choseRandomY() {
		return new Random().nextInt(Map.getInstance().getSize_y());
	}
	
	private boolean isPossibleToPutBonus(int x, int y) {
		return !isAlreadyPut(x, y) && !isWall(x, y);
	}
	
	private boolean isAlreadyPut(int x, int y) {
		for(InfoItem bonus : Map.getInstance().getStart_Items()) {
			if(bonus.getX() == x && bonus.getY() == y)
				return true;
		}
		return false;
	}
	
	private boolean isWall(int x, int y) {
		return Map.getInstance().getWalls()[x][y] || Map.getInstance().getStart_brokable_walls()[x][y];
	}
}
