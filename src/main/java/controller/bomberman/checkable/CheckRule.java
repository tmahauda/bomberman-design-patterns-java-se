package controller.bomberman.checkable;

import java.io.Serializable;

/**
 * Regle commun au jeu Bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public abstract class CheckRule implements Checkable, Serializable {
	
	private static final long serialVersionUID = 1L;

	public CheckRule() {
		
	}
}
