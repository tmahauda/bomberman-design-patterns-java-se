package controller.bomberman.checkable;

/**
 * Enumération des règles implémentées
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public enum RuleType {
	APPLY_BONUS, ATTACK_BOMB, ATTACK_ENNEMY, FLY_BIRD, ENNEMY_MOVE_RANDOMLY, PUT_BOMB_RANDOMLY;
}
