package controller.bomberman.checkable;

import java.util.Random;
import controller.bomberman.moveable.AgentMove;
import model.bomberman.agent.enemy.AgentEnemy;
import model.bomberman.map.Map;

/**
 * Déplacer les ennemis aléatoirement sur le plateau
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class CheckRuleMoveAgentEnemyRandomly extends CheckRule {
	
	private static final long serialVersionUID = 1L;

	public CheckRuleMoveAgentEnemyRandomly() {

	}

	@Override
	public boolean checkRule() {
		return true;
	}
	
	/**
	 * Déplacer les ennemis aléatoirements
	 */
	@Override
	public void executeRule() {
		for(AgentEnemy ennemy : Map.getInstance().getAgentsEnnemy()) {
			AgentMove randomMove = this.choseRandomMove();
			ennemy.doMove(randomMove);
		}
	}

	/**
	 * Choisir une action aléatoirement
	 * @return l'action tirée aléatoirement
	 */
	private AgentMove choseRandomMove() {
		int maxAction = AgentMove.values().length;
		int randomMove = new Random().nextInt(maxAction);
		
		return AgentMove.values()[randomMove];
	}
}
