package controller.bomberman.checkable;

import controller.bomberman.actionnable.AgentAction;
import model.bomberman.agent.enemy.AgentEnemy;
import model.bomberman.map.Map;

/**
 * Vérifier si un ennemi a attaqué un agent bomberman
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class CheckRuleAttackEnnemy extends CheckRule {

	private static final long serialVersionUID = 1L;

	public CheckRuleAttackEnnemy() {

	}

	@Override
	public boolean checkRule() {
		return true;
	}
	
	@Override
	public void executeRule() {
		//Pour chaque ennemi
		for(AgentEnemy ennemy : Map.getInstance().getAgentsEnnemy()) {
			ennemy.doAction(AgentAction.ATTACK);
			if(Map.getInstance().getCurrentAgentKind().isDead()) {
				Map.getInstance().getAgents().remove(Map.getInstance().getCurrentAgentKind());
				Map.getInstance().getAgentsKind().remove(Map.getInstance().getCurrentAgentKind());
				Map.getInstance().getStart_Agents().remove(Map.getInstance().getCurrentAgentKind());	
			}
		}
	}
}