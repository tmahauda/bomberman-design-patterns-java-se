package controller.bomberman.checkable;

/**
 * Effectuer une vérification d'une regle de jeu
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public interface Checkable {
	
	/**
	 * Vérifier si une règle peut être exécuter
	 * @return
	 */
	public boolean checkRule();
	
	/**
	 * La règle à exécuter
	 */
	public void executeRule();
}
