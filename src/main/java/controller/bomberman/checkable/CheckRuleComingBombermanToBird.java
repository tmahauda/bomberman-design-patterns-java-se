package controller.bomberman.checkable;

import controller.bomberman.moveable.AgentMoveableJump;
import controller.bomberman.moveable.AgentMoveableStill;
import model.bomberman.agent.enemy.AgentEnemy;
import model.bomberman.agent.enemy.AgentEnemyBird;
import model.bomberman.agent.kind.AgentKind;
import model.bomberman.map.Map;

/**
 * Vérifier si un agent bomberman se rapproche d'un agent bird
 * Si oui l'oiseau s'éloigne
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 * 
 */
public class CheckRuleComingBombermanToBird extends CheckRule {

	private static final long serialVersionUID = 1L;

	public CheckRuleComingBombermanToBird() {

	}

	@Override
	public boolean checkRule() {
		return true;
	}
	
	@Override
	public void executeRule() {
		for(AgentEnemy agent : Map.getInstance().getAgentsEnnemy()) {
			if(!(agent instanceof AgentEnemyBird)) continue;
			
			for(AgentKind kind : Map.getInstance().getAgentsKind()) {
				this.flyOrStillBird((AgentEnemyBird)agent, kind);
			}
		}
	}
	
	/**
	 * Deux stratégies :
	 * - Soit il s'éloigne en sautant par dessus les murs
	 * - Soit l'oiseau reste immobile dans le cas où il n'y a pas d'agent bomberman
	 * @param bird
	 * @param kind
	 */
	private void flyOrStillBird(AgentEnemyBird bird, AgentKind kind) {
		//On récupère les coordonnées de l'agent bird
		int agentBirdX = bird.getX();
		int agentBirdY = bird.getY();
		
		//On récupère les coordonnées de l'agent bomberman
		int agentBombX = kind.getX();
		int agentBombY = kind.getY();
		
		//Périmétre où il faut réveiller le bird
		int range = 2;
		
		//Si l'agent bomberman est dans le perimètre de l'agent bird, dans ce cas bird peut se déplacer
		//Sinon bird reste immobile
		if(Math.abs(agentBirdX - range) <= agentBombX && agentBombX <= Math.abs(agentBirdX + range)
				&& Math.abs(agentBirdY - range) <= agentBombY && agentBombY <= Math.abs(agentBirdY + range)) {
			
			//On change de stratégie de déplacement pour l'agent bird : il peut se déplacer en sautant par dessus les murs
			bird.setMove(new AgentMoveableJump());
			
		} else {
			//Sinon il reste immobile
			bird.setMove(new AgentMoveableStill());
		}
	}
}
