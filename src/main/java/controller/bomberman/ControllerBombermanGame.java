package controller.bomberman;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import controller.Controller;
import controller.bomberman.actionnable.AgentAction;
import controller.bomberman.checkable.CheckRuleMoveAgentEnemyRandomly;
import controller.bomberman.checkable.RuleType;
import controller.bomberman.moveable.AgentMove;
import ia.search.FactorySearch;
import ia.search.Node;
import ia.search.explorable.Explorable;
import ia.search.explorable.ExplorationA;
import ia.solve.heuristic.Heuristic;
import ia.solve.heuristic.HeuristicBombermanManhattan;
import model.Game;
import model.bomberman.map.Map;
import model.bomberman.map.MapAdapter;
import view.ViewCommand;
import view.ViewGameOver;
import view.ViewWin;
import view.bomberman.ViewBomberman;

/**
 * Controlleur qui permet d'assurer les contrôles du jeu quand des actions ont été effectuées par
 * l’utilisateur dans la Vue (par exemple lorsque l’utilisateur clique sur un bouton)
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class ControllerBombermanGame extends Controller<MapAdapter> {
	
	/**
	 * La vue bomberman
	 */
	private ViewBomberman viewBomberman;	
	
	/**
	 * La vue commande permettant de controller la vue bomberman
	 */
	private ViewCommand vc;
	
	public ControllerBombermanGame(Game<MapAdapter> game) {
		super(game);
		this.vc = new ViewCommand(this, this.game);		 
	}
	
	/**
	 * Affiche une vue lorsque la partie est terminé
	 * - Soit gagné et passer au niveau suivant
	 * - Soit perdu et afficher gameover
	 */
	public void dispalyGameOver() {		
		if(this.game.gameOver()) {
			new ViewGameOver(this, this.game);		
		}else {
			new ViewWin(this, this.game);
		}
	}
	
	@Override
	public void init() {
		
		//On la fenetre bomberman est active
		if(this.close()) {
			this.vc.getButtonStart().setEnabled(true);
			this.vc.getButtonRun().setEnabled(false);
			this.vc.getButtonStep().setEnabled(false);
			this.vc.getButtonStop().setEnabled(false);
			this.vc.getSliderTurnPerSecond().setEnabled(true);	
			this.vc.getButtonChoose().setEnabled(true);
			this.vc.getComboBoxLayout().setEnabled(true);
			this.vc.getButtonSolve().setEnabled(false);	
			this.vc.getButtonSave().setEnabled(false);	
		} else {
			//On active uniquement le bouton pour sélectionner un fichier
			this.vc.getButtonStart().setEnabled(false);
			this.vc.getButtonRun().setEnabled(false);
			this.vc.getButtonStep().setEnabled(false);
			this.vc.getButtonStop().setEnabled(false);
			this.vc.getSliderTurnPerSecond().setEnabled(true);	
			this.vc.getButtonChoose().setEnabled(true);
			this.vc.getComboBoxLayout().setEnabled(false);
			this.vc.getButtonSolve().setEnabled(false);	
			this.vc.getButtonSave().setEnabled(false);
		}
	}

	@Override
	public void start() {
		
		//On ferme la fenetre si déjà active
		this.close();
		
		//On charge la map
		try {
			Map.getInstance().load();			
		} catch(Exception e) {
			e.printStackTrace();
			return;
		}
		
		//On initialise le jeu = les agents
		this.game.init();
		
		//On initialise la solution
		this.startSoluce();
		
		//On affiche la vue
		this.viewBomberman = new ViewBomberman(this, this.game);
		Map.getInstance().setViewBomberman(this.viewBomberman);
		
		//On peut 
		//- démarrer le jeu 
		//- changer le temps entre chaque tour
		this.vc.getButtonStart().setEnabled(false);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);	
		this.vc.getButtonChoose().setEnabled(false);
		this.vc.getComboBoxLayout().setEnabled(false);
		this.vc.getButtonSave().setEnabled(false);
		this.vc.getButtonSolve().setEnabled(true);	
	}
	
	@Override
	public void startSoluce() {
		//On enleve la règle de déplacement aléatoire des ennemis dans la résolution
		this.game.getRules().remove(RuleType.ENNEMY_MOVE_RANDOMLY);
		
		//On sauvegarde l'instance unique 
		Map singleton = MapAdapter.getInstance();
		
		//On l'utilise dans l'ordre la stratégie suivante
		//1 = haut
		//2 = bas
		//3	= gauche
		//4 = droite
		//5 = poser une bombe
		List<Integer> actions = new ArrayList<>();
		actions.add(1);
		actions.add(2);
		actions.add(3);
		actions.add(4);
		actions.add(5);
		
		//On utilise l'alorithme A* pour résoudre le probleme
		Explorable<MapAdapter> frontier = new ExplorationA<>();
		
		//On l'utilise l'heuristique de Manhanttan
		Heuristic<MapAdapter> heuristic = new HeuristicBombermanManhattan();
		
		Node<MapAdapter> find = FactorySearch.getSoluceBomberman(this.game.getMaxturn(), this.game.getTime(), 
		Map.getInstance().getFilename(), actions, frontier, heuristic);
				
		//On récupère tous les noeuds pour résoudre le problème si la solution a été trouvé
		//Si toutefois la solution n'a pas été trouvé dans le cas le joueur ne peut pas réussir à le résoudre
		//Il faut donc afficher un message d'alerte
		LinkedList<Node<MapAdapter>> soluces = new LinkedList<>();
		if(find != null)
			soluces = find.getAllNodes();
				
		//On réinjecte le singleton
		MapAdapter.setInstance(singleton);
		
		//On remet la règle de déplacement aléatoire des ennemis pour le jeu
		this.game.getRules().put(RuleType.ENNEMY_MOVE_RANDOMLY, new CheckRuleMoveAgentEnemyRandomly());
		
		//On donne la liste des solutions au jeu
		this.game.setSoluces(soluces);
	}
	

	@Override
	public void step(boolean soluce) {
		this.game.step(soluce);
		
		//On peut 
		//- réinitialiser le jeu
		//- reprendre le jeu
		//- effectuer un seul tour
		//- modifier le temps entre chaque tour
		//- changer de map entre temps
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(true);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
		this.vc.getButtonChoose().setEnabled(true);
		this.vc.getComboBoxLayout().setEnabled(true);
		this.vc.getButtonSave().setEnabled(false);
		this.vc.getButtonSolve().setEnabled(true);	
	}

	@Override
	public void run(boolean soluce) {
		if(soluce) {
			//On enleve la règle de déplacement aléatoire des ennemis dans la résolution
			this.game.getRules().remove(RuleType.ENNEMY_MOVE_RANDOMLY);
		}
		this.game.launch(soluce);
		
		//On peut 
		//- mettre en pause le jeu
		//- changer le temps entre chaque tour
		this.vc.getButtonStart().setEnabled(false);
		this.vc.getButtonRun().setEnabled(false);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(true);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
		this.vc.getButtonChoose().setEnabled(false);
		this.vc.getComboBoxLayout().setEnabled(false);
		this.vc.getButtonSave().setEnabled(false);
		this.vc.getButtonSolve().setEnabled(false);	
	}

	@Override
	public void stop() {
		this.game.stop();
		
		//On peut 
		//- réinitialiser le jeu
		//- reprendre le jeu
		//- effectuer un seul tour
		//- modifier le temps entre chaque tour
		//- changer de map entre temps
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(true);
		this.vc.getButtonStep().setEnabled(true);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
		this.vc.getButtonChoose().setEnabled(true);
		this.vc.getComboBoxLayout().setEnabled(true);
		this.vc.getButtonSave().setEnabled(true);
		this.vc.getButtonSolve().setEnabled(true);	
	}

	@Override
	public void setTime(long time) {
		this.game.setTime(time*1000);
	}
	
	/**
	 * Méthode qui permet de sauvegarder le jeu
	 */
	/**
	 * Méthode qui permet de sauvegarder le jeu
	 */
	@SuppressWarnings("resource")
	@Override
	public void save() {
		// Serialization
		try
		{      
			//Saving of object in a file
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");  
			LocalDateTime now = LocalDateTime.now();  
			//FileOutputStream file=new FileOutputStream("./Sauvegarde/niveau1/Save"+dtf.format(now));
			System.out.println("Niveau : " + Map.getInstance().getNiveau());  
			FileOutputStream file = null;
			switch(Map.getInstance().getNiveau()) {		
			
			case "niveau1":
				file = new FileOutputStream("./Sauvegarde/niveau1/Save"+dtf.format(now));	
				break;
			case "niveau2":
				file = new FileOutputStream("./Sauvegarde/niveau2/Save"+dtf.format(now));				
				break;
			case "niveau3":
				file = new FileOutputStream("./Sauvegarde/niveau3/Save"+dtf.format(now));
				break;	
			default:
				System.out.println("le niveau n'existe pas");
			}
						
			ObjectOutputStream out = new ObjectOutputStream(file);			
			 
			//Method for serialization of object
			out.writeObject(Map.getInstance());			
			
			out.close();
			file.close();
		} catch(IOException ex) {
			System.out.println("IOException is caught" + ex.getMessage());
		}
	}
	
	/**
	 * Méthode qui charge le plateau à partir du fichier sauvagarder.
	 * @param niveau le niveau du jeu
	 * @param fileName le nom du fichier sauvegarder
	 */
	public void chargerPlateauSauvegarder(String niveau, String fileName) {
		Map loaded_file = null;
		try{
			// Reading the object from a file
			FileInputStream file = new FileInputStream("./Sauvegarde/"+niveau+"/"+fileName);
			ObjectInputStream in = new ObjectInputStream(file);
			
			// Method for deserialization of object
			loaded_file = (Map)in.readObject();			
			in.close();
			file.close();
			System.out.println("Object has been deserialized");
			System.out.println("Loaded " + loaded_file.getFilename());
			System.out.println("Loaded " + loaded_file.getNiveau());
			System.out.println("Loaded " + loaded_file.getSize_x());
		} catch(IOException ex){
			System.out.println("IOException is caught");
		} catch(ClassNotFoundException ex){
			System.out.println("ClassNotFoundException is caught");
		}
		
		
		//On charge la map
		try {
			Map.getInstance().setFilename(loaded_file.getFilename());
			Map.getInstance().setAgents(loaded_file.getAgents());
			Map.getInstance().setAgentsEnnemy(loaded_file.getAgentsEnnemy());
			Map.getInstance().setAgentsKind(loaded_file.getAgentsKind());
			Map.getInstance().setCurrentAgentKind(loaded_file.getCurrentAgentKind());
			Map.getInstance().setNiveau(loaded_file.getNiveau());
			Map.getInstance().setSize_x(loaded_file.getSize_x());
			Map.getInstance().setSize_y(loaded_file.getSize_y());
			Map.getInstance().setStart_Agents(loaded_file.getStart_Agents());
			Map.getInstance().setStart_Bombs(loaded_file.getStart_Bombs());
			Map.getInstance().setStart_brokable_walls(loaded_file.getStart_brokable_walls());
			Map.getInstance().setStart_Items(loaded_file.getStart_Items());
			Map.getInstance().setWalls(loaded_file.getWalls());
			
			Map.getInstance().getCurrentAgentKind().setX(loaded_file.getCurrentAgentKind().getX());
			Map.getInstance().getCurrentAgentKind().setY(loaded_file.getCurrentAgentKind().getY());
			
			Map.getInstance().load();
			Map.getInstance().setViewBomberman(new ViewBomberman(this, this.game));		

			
		} catch(Exception e) {			
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Méthode qui charge le chemin absolue du layout.lay.
	 * @param path le chemin du layout
	 */
	public void loadMap(String path) {
		if(path == null) return;
		
		System.out.println("Controller load map : " + path);
		Map.getInstance().setNiveau(path.substring(path.length()-11, path.length()-4));
		System.out.println("Niveau : "+Map.getInstance().getNiveau());
		
		//On ferme la fenetre si déjà active
		this.close();
		
		//On enregistre le chemin de fichier
		Map.getInstance().setFilename(path);	
		
		//On peut initialiser le jeu ou changer de map
		this.vc.getButtonStart().setEnabled(true);
		this.vc.getButtonRun().setEnabled(false);
		this.vc.getButtonStep().setEnabled(false);
		this.vc.getButtonStop().setEnabled(false);
		this.vc.getSliderTurnPerSecond().setEnabled(true);
		this.vc.getButtonChoose().setEnabled(true);
		this.vc.getComboBoxLayout().setEnabled(true);
		this.vc.getButtonSave().setEnabled(false);
		this.vc.getButtonSolve().setEnabled(false);	
	}
	
	/**
	 * Méthode qui permet d'effectuer une action sur un agent bomberman
	 * @param action l'action a effectuer
	 */
	public void doAction(AgentAction action) {
		Map.getInstance().getCurrentAgentKind().doAction(action);
	}
	
	/**
	 * Méthode qui permet d'effectuer un mouvement sur un agent bomberman
	 * @param move le mouvement à effectuer
	 */
	public void doMove(AgentMove move) {
		Map.getInstance().getCurrentAgentKind().doMove(move);	
	}

	/**
	 * Fermer la fenetre du jeu si active
	 * @return
	 */
	public boolean close() {
		//On ferme la fenetre si déjà active
		if(this.viewBomberman != null) {
			this.viewBomberman.dispose();
			this.viewBomberman = null;
			return true;
		} else return false;
	}
	
	/**
	 * @return the viewBomberman
	 */
	public ViewBomberman getViewBomberman() {
		return viewBomberman;
	}

	/**
	 * @param viewBomberman the viewBomberman to set
	 */
	public void setViewBomberman(ViewBomberman viewBomberman) {
		this.viewBomberman = viewBomberman;
	}

	/**
	 * @return the vc
	 */
	public ViewCommand getVc() {
		return vc;
	}

	/**
	 * @param vc the vc to set
	 */
	public void setVc(ViewCommand vc) {
		this.vc = vc;
	}
}
