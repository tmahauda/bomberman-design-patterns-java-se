package ia.solve;

import java.util.ArrayList;
import java.util.List;

/**
 * Générer toutes les combinaisons d'actions à effectuer dans l'ordre
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class FactoryAction {

	private static List<List<Integer>> combinaisons = new ArrayList<>();
	
    public static void generateAllCombinaisons(int[] a, int k) {
        if (k == a.length) {
        	List<Integer> combinaison = new ArrayList<>();
            for (int i = 0; i < a.length; i++) {
            	combinaison.add(a[i]);
            }
            combinaisons.add(combinaison);
        } 
        else {
            for (int i = k; i < a.length; i++) {
                int temp = a[k];
                a[k] = a[i];
                a[i] = temp;
 
                generateAllCombinaisons(a, k + 1);
                
                temp = a[k];
                a[k] = a[i];
                a[i] = temp;
            }
        }
    }

	/**
	 * @return the combinaisons
	 */
	public static List<List<Integer>> getCombinaisons() {
		return combinaisons;
	}

	/**
	 * @param combinaisons the combinaisons to set
	 */
	public static void setCombinaisons(List<List<Integer>> combinaisons) {
		FactoryAction.combinaisons = combinaisons;
	}
}
