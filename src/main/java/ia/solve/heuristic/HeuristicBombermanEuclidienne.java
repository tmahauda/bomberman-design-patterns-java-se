package ia.solve.heuristic;

import model.bomberman.agent.Agent;

/**
 * Calcul de la distance Euclidienne
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class HeuristicBombermanEuclidienne extends HeuristicBomberman {

	public HeuristicBombermanEuclidienne() {

	}

	@Override
	public double calculDistance(Agent a, Agent b) {
		return Math.sqrt(Math.pow((a.getX()-b.getX()),2) + Math.pow((a.getY()-b.getY()),2));
	}

	@Override
	public String toString() {
		return "HeuristicBombermanEuclidienne [h=" + h + ", g=" + g + ", f=" + f + ", getH()=" + getH() + ", getG()="
				+ getG() + ", getF()=" + getF() + "]";
	}
}
