package ia.solve.heuristic;

import model.bomberman.agent.Agent;

/**
 * Calcul de la distance de Manhattan
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class HeuristicBombermanManhattan extends HeuristicBomberman {

	public HeuristicBombermanManhattan() {

	}

	@Override
	public double calculDistance(Agent a, Agent b) {
		return Math.abs(a.getX()-b.getX()) + Math.abs(a.getY()-b.getY());
	}

	@Override
	public String toString() {
		return "HeuristicBombermanManhattan [h=" + h + ", g=" + g + ", f=" + f + ", getH()=" + getH() + ", getG()="
				+ getG() + ", getF()=" + getF() + "]";
	}
}
