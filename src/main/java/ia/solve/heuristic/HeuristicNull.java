package ia.solve.heuristic;

import ia.search.Node;
import ia.solve.Solveable;

/**
 * Heuristique null pour les explorations qui n'utilisent pas d'heuristique
 *
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 * Created on : 22/12/2019
 *
 */
public class HeuristicNull<T extends Solveable> extends Heuristic<T> {

	public HeuristicNull() {
		
	}

	@Override
	public double calculHHeuristic(Node<T> n) {
		return 0;
	}

	@Override
	public double calculGHeuristic(Node<T> n) {
		return 0;
	}
	
	@Override
	public String toString() {
		return "HeuristicNull";
	}
}
